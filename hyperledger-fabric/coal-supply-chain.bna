PK
     OM5F�a;  ;     package.json{"engines":{"composer":"^0.20.0"},"name":"coal-supply-chain","version":"0.0.1","description":"Smart contract for coal supply chain","scripts":{"prepublish":"mkdirp ./dist && composer archive create --sourceType dir --sourceName . -a ./dist/coal-supply-chain.bna","pretest":"npm run lint","lint":"eslint .","test":"nyc mocha -t 0 test/*.js && cucumber-js"},"keywords":["composer","composer-network"],"author":"bufo-innovations","email":"developers@bufoinnovations.com","license":"Apache-2.0","devDependencies":{"composer-admin":"^0.20.0","composer-cli":"^0.20.0","composer-client":"^0.20.0","composer-common":"^0.20.0","composer-connector-embedded":"^0.20.0","composer-cucumber-steps":"^0.20.0","chai":"latest","chai-as-promised":"latest","cucumber":"^2.2.0","eslint":"latest","nyc":"latest","mkdirp":"latest","mocha":"latest"}}PK
     OM��rm:   :   	   README.md# coal-supply-chain

Smart contract for coal supply chain
PK
     OMJΉ�'  '     permissions.acl/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

rule NetworkAdminUser {
    description: "Grant business network administrators full access to user resources"
    participant: "org.hyperledger.composer.system.NetworkAdmin"
    operation: ALL
    resource: "**"
    action: ALLOW
}

rule NetworkAdminSystem {
    description: "Grant business network administrators full access to system resources"
    participant: "org.hyperledger.composer.system.NetworkAdmin"
    operation: ALL
    resource: "org.hyperledger.composer.system.**"
    action: ALLOW
}

PK
     OM               models/PK
     OM�ZZ�X  X     models/models.cto/**
* Coal logistic business network definition.
*/

namespace org.coal_logistics.basic

asset consignment identified by consignmentId {
  o String consignmentId
  o String content optional
  o weight orderedQuantity
  o location destination
  o String orderedQualityGrade
  --> organization currentHandler
  --> organization initiator
  o DateTime initiateTime
  o status currentStatus
  o weight[] weightList
  o location[] locationList
  o quality[] qualityList
  o action[] actionList
}

concept quality {
  o Double ash
  o Double moisture
  o Double volatility
  o Double FC
  o Double gcv
  o String grade
  --> auditor auditedBy
  o DateTime auditTime
}

concept weight {
  o Double value
  o String unit default = "tons"
  o DateTime fetchTime
  o location weighLocation optional
}

concept location {
  o String latitude
  o String longitude
  o String name optional
  o DateTime fetchTime optional
}

enum status {
  o initiated_by_consumer
  o acknowledged_by_miner
  o dispatched_by_miner
  o audited_by_miner
  o acknowledged_by_transporter
  o in_transit
  o update_location
  o update_weight
  o received_by_consumer
  o audited_by_consumer
  o completed
  o failed
}
  
concept action {
  o status Status
  o DateTime transactionTime
  o String comment optional
}

participant auditor identified by auditorId {
  o String auditorId
  o String name
  --> organization appointedBy
}

participant organization identified by orgId {
  o String orgId
  o String name
  o orgType role
  o location Location
}

enum orgType {
  o consumer
  o miner
  o transporter
}

transaction createConsignment {
  o String consignmentId
  o String content
  o String orderedQualityGrade
  o weight orderedQuantity
  --> organization initiator
}

transaction acknowledgeByMiner {
  --> consignment Consignment
  --> organization Organization
}

transaction dispatchByMiner {
  --> organization Organization
  --> consignment Consignment
  o weight dispatchedWeight
}

transaction inspectQualityByMiner {
  --> consignment Consignment
  --> auditor Auditor
  o quality Quality
}
  
transaction inspectQualityByConsumer {
  --> consignment Consignment
  --> auditor Auditor
  o quality Quality
}
  
transaction acknowledgeByTransporter {
  --> consignment Consignment
  --> organization Organization
  o weight MeasuredWeight
}
  
transaction shippedByTransporter {
  --> consignment Consignment
  --> organization Organization
  o weight measuredWeight
  o location shippedLocation
}
  
transaction receivedByConsumer {
  --> consignment Consignment
  --> organization Organization
  o weight receivedWeight
}

transaction updateWeight {
  --> consignment Consignment
  o weight measuredWeight
}

transaction validate {
  --> consignment Consignment
  --> organization Organization
}

event notification {
  --> consignment Consignment optional
  --> organization publishedBy optional
  o String message
}


PK
     OM               lib/PK
     OM0Qe�  �     lib/logic.js/**
* Create new consignment processor function.
* @param {org.coal_logistics.basic.createConsignment} tx The sample transaction instance.
* @transaction
*/

async function createConsignment(tx) {
  const currentTime = new Date();
  const consignmentId = tx.consignmentId;
  const content = tx.content
  const transactionTime = currentTime;
  const initiator = tx.initiator;
  const orderedQuantity = tx.orderedQuantity;
  const orderedQualityGrade = tx.orderedQualityGrade

  // check if consignment is created by consumer
  if(initiator.role == 'consumer'){

    // create new consignment
    const factory = getFactory();
    const consignment = factory.newResource('org.coal_logistics.basic', 'consignment', consignmentId);
    consignment.currentStatus = "initiated_by_consumer";
    consignment.content = content;
    consignment.currentHandler = initiator;
    consignment.initiator = initiator;
    consignment.orderedQuantity = orderedQuantity;
    consignment.weightList = [];
    consignment.locationList = [];
    consignment.qualityList = [];
    consignment.actionList = [];
    consignment.destination = initiator.Location;
    consignment.orderedQualityGrade = orderedQualityGrade;
    consignment.initiateTime = transactionTime;
    // add the contracts
    const consignmentRegistry = await getAssetRegistry('org.coal_logistics.basic.consignment');
    await consignmentRegistry.addAll([consignment]);

    // emit message notifying new consignment
    let event = factory.newEvent('org.coal_logistics.basic', 'notification');
    event.publishedBy = initiator;
    event.Consignment = consignment;
    event.message = "New consignment (ID: " + consignmentId + ") created by " + initiator.name + " at " +                          transactionTime;
    emit(event);
  }
  else {
    // new consignment can be created only by consumer organization
    console.log("Must be requested by consumer org, not by " + initiator.type)
    throw new Error("Consignment can only be initiated by consumer");
  }
}

/**
* Acknowledge consignment request processor function.
* @param {org.coal_logistics.basic.acknowledgeByMiner} tx The sample transaction instance.
* @transaction
*/

async function acknowledgeByMiner(tx) {
  const transactionTime = new Date();
  const consignment = tx.Consignment;
  const organization = tx.Organization;

  // consignment can be acknowledged only by miner
  if (organization.role == 'miner') {
    // update status
    consignment.currentStatus = "acknowledged_by_miner";
    consignment.currentHandler = organization;
    // append into actionList
    let action = {};
    action.Status = "acknowledged_by_miner";
    action.transactionTime = transactionTime;
    consignment.actionList.push(action);
    const consignmentRegistry = await getAssetRegistry('org.coal_logistics.basic.consignment');
    await consignmentRegistry.update(consignment);

    // emit message notifying acknowledgement of consignment by miner
    const factory = getFactory();
    let event = factory.newEvent('org.coal_logistics.basic', 'notification');
    event.publishedBy = organization;
    event.Consignment = consignment;
    event.message = "Consignment (ID: " + consignment.consignmentId + ") acknowledged by " + organization.name + " at " +                          transactionTime;
    emit(event);
  }
  else {
    throw new Error ("Action is available only to miner");
  }
}


/**
* Create new consignment processor function.
* @param {org.coal_logistics.basic.dispatchByMiner} tx The sample transaction instance.
* @transaction
*/

async function dispatchByMiner(tx) {
  const transactionTime = new Date();
  const consignment = tx.Consignment;
  const organization = tx.Organization;

  // consignment can be dispatched only by miner
  if (organization.role == 'miner') {
    // update status
    consignment.currentStatus = "dispatched_by_miner";
    consignment.currentHandler = organization;
    // append into actionList
    let action = {};
    action.Status = "dispatched_by_miner";
    action.transactionTime = transactionTime;
    consignment.actionList.push(action);
    const consignmentRegistry = await getAssetRegistry('org.coal_logistics.basic.consignment');
    await consignmentRegistry.update(consignment);

    // emit message notifying acknowledgement of consignment by miner
    const factory = getFactory();
    let event = factory.newEvent('org.coal_logistics.basic', 'notification');
    event.publishedBy = organization;
    event.Consignment = consignment;
    event.message = "Consignment (ID: " + consignment.consignmentId + ") acknowledged by " + organization.name + " at " +                          transactionTime;
    emit(event);
  }
  else {
    throw new Error ("Action is available only to miner");
  }
}

/**
* Create new consignment processor function.
* @param {org.coal_logistics.basic.inspectQualityByMiner} tx The sample transaction instance.
* @transaction
*/

async function inspectQualityByMiner(tx) {
  const transactionTime = new Date();
  const consignment = tx.Consignment;
  const auditor = tx.Auditor;
  const quality = tx.Quality;
  const auditorAppointedBy = auditor.appointedBy;
  if (auditorAppointedBy.role == "miner") {
    // apend into qualityList
    consignment.qualityList.push(quality);
  	// append into actionList
    let action = {};
    action.transactionTime = TransactionTime;
    action.Status = "audited_by_miner";
    consignment.action.push(action)
    const consignmentRegistry = await getAssetRegistry('org.coal_logistics.basic.consignment');
    await consignmentRegistry.update(consignment);
  }
  else{
    throw new Error ("Action is available only to auditors assigned by miner");
  }
}

/**
* Create new consignment processor function.
* @param {org.coal_logistics.basic.inspectQualityByConsumer} tx The sample transaction instance.
* @transaction
*/

async function inspectQualityByConsumer(tx) {
  const transactionTime = new Date();
  const consignment = tx.Consignment;
  const auditor = tx.Auditor;
  const quality = tx.Quality;
  const auditorAppointedBy = auditor.appointedBy;
  if (auditorAppointedBy.role == "consumer") {
    // apend into qualityList
    consignment.qualityList.push(quality);
  	// append into actionList
    let action = {};
    action.transactionTime = TransactionTime;
    action.Status = "audited_by_consumer";
    consignment.action.push(action);
    const consignmentRegistry = await getAssetRegistry('org.coal_logistics.basic.consignment');
    await consignmentRegistry.update(consignment);
  }
  else{
    throw new Error ("Action is available only to auditors assigned by consumner");
  }
}

PK 
     OM5F�a;  ;                   package.jsonPK 
     OM��rm:   :   	             e  README.mdPK 
     OMJΉ�'  '               �  permissions.aclPK 
     OM                          models/PK 
     OM�ZZ�X  X               ?  models/models.ctoPK 
     OM                        �  lib/PK 
     OM0Qe�  �               �  lib/logic.jsPK      �  .    