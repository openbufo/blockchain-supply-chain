/**
* Create new consignment processor function.
* @param {org.coal_logistics.basic.createConsignment} tx The sample transaction instance.
* @transaction
*/

async function createConsignment(tx) {
  const currentTime = new Date();
  const consignmentId = tx.consignmentId;
  const content = tx.content
  const transactionTime = currentTime;
  const initiator = tx.initiator;
  const orderedQuantity = tx.orderedQuantity;
  const orderedQualityGrade = tx.orderedQualityGrade

  // check if consignment is created by consumer
  if(initiator.role == 'consumer'){

    // create new consignment
    const factory = getFactory();
    const consignment = factory.newResource('org.coal_logistics.basic', 'consignment', consignmentId);
    consignment.currentStatus = "initiated_by_consumer";
    consignment.content = content;
    consignment.currentHandler = initiator;
    consignment.initiator = initiator;
    consignment.orderedQuantity = orderedQuantity;
    consignment.weightList = [];
    consignment.locationList = [];
    consignment.qualityList = [];
    consignment.actionList = [];
    consignment.destination = initiator.Location;
    consignment.orderedQualityGrade = orderedQualityGrade;
    consignment.initiateTime = transactionTime;
    
    // add the contracts
    const consignmentRegistry = await getAssetRegistry('org.coal_logistics.basic.consignment');
    await consignmentRegistry.addAll([consignment]);

    // emit message notifying new consignment
    let event = factory.newEvent('org.coal_logistics.basic', 'notification');
    event.publishedBy = initiator;
    event.Consignment = consignment;
    event.message = "New consignment (ID: " + consignmentId + ") created by " + initiator.name + " at " +                          transactionTime;
    emit(event);
  }
  else {
    // new consignment can be created only by consumer organization
    console.log("Must be requested by consumer org, not by " + initiator.type)
    throw new Error("Consignment can only be initiated by consumer");
  }
}

/**
* Acknowledge consignment request processor function.
* @param {org.coal_logistics.basic.acknowledgeByMiner} tx The sample transaction instance.
* @transaction
*/

async function acknowledgeByMiner(tx) {
  const transactionTime = new Date();
  const consignment = tx.Consignment;
  const organization = tx.Organization;

  // consignment can be acknowledged only by miner
  if (organization.role == 'miner') {
    // update status
    consignment.currentStatus = "acknowledged_by_miner";
    consignment.currentHandler = organization;
    
    // append into actionList
    let action = {};
    action.Status = "acknowledged_by_miner";
    action.transactionTime = transactionTime;
    consignment.actionList.push(action);
    
    const consignmentRegistry = await getAssetRegistry('org.coal_logistics.basic.consignment');
    await consignmentRegistry.update(consignment);

    // emit message notifying acknowledgement of consignment by miner
    const factory = getFactory();
    let event = factory.newEvent('org.coal_logistics.basic', 'notification');
    event.publishedBy = organization;
    event.Consignment = consignment;
    event.message = "Consignment (ID: " + consignment.consignmentId + ") acknowledged by " + organization.name + " at " +                          transactionTime;
    emit(event);
  }
  else {
    throw new Error ("Action is available only to miner");
  }
}


/**
* Create new consignment processor function.
* @param {org.coal_logistics.basic.dispatchByMiner} tx The sample transaction instance.
* @transaction
*/

async function dispatchByMiner(tx) {
  const transactionTime = new Date();
  const consignment = tx.Consignment;
  const organization = tx.Organization;

  // consignment can be dispatched only by miner
  if (organization.role == 'miner') {
    // update status
    consignment.currentStatus = "dispatched_by_miner";
    consignment.currentHandler = organization;
    
    // append into actionList
    let action = {};
    action.Status = "dispatched_by_miner";
    action.transactionTime = transactionTime;
    consignment.actionList.push(action);
    
    const consignmentRegistry = await getAssetRegistry('org.coal_logistics.basic.consignment');
    await consignmentRegistry.update(consignment);

    // emit message notifying acknowledgement of consignment by miner
    const factory = getFactory();
    let event = factory.newEvent('org.coal_logistics.basic', 'notification');
    event.publishedBy = organization;
    event.Consignment = consignment;
    event.message = "Consignment (ID: " + consignment.consignmentId + ") acknowledged by " + organization.name + " at " +                          transactionTime;
    emit(event);
  }
  else {
    throw new Error ("Action is available only to miner");
  }
}

/**
* Create new consignment processor function.
* @param {org.coal_logistics.basic.inspectQualityByMiner} tx The sample transaction instance.
* @transaction
*/

async function inspectQualityByMiner(tx) {
  const transactionTime = new Date();
  const consignment = tx.Consignment;
  const auditor = tx.Auditor;
  const quality = tx.Quality;
  const auditorAppointedBy = auditor.appointedBy;
  if (auditorAppointedBy.role == "miner") {
    // apend into qualityList
    consignment.qualityList.push(quality);
  	// append into actionList
    let action = {};
    action.transactionTime = TransactionTime;
    action.Status = "audited_by_miner";
    consignment.action.push(action)
    
    const consignmentRegistry = await getAssetRegistry('org.coal_logistics.basic.consignment');
    await consignmentRegistry.update(consignment);
  }
  else{
    throw new Error ("Action is available only to auditors assigned by miner");
  }
}

/**
* Create new consignment processor function.
* @param {org.coal_logistics.basic.inspectQualityByConsumer} tx The sample transaction instance.
* @transaction
*/

async function inspectQualityByConsumer(tx) {
  const transactionTime = new Date();
  const consignment = tx.Consignment;
  const auditor = tx.Auditor;
  const quality = tx.Quality;
  const auditorAppointedBy = auditor.appointedBy;
  if (auditorAppointedBy.role == "consumer") {
    // apend into qualityList
    consignment.qualityList.push(quality);
  	// append into actionList
    let action = {};
    action.transactionTime = TransactionTime;
    action.Status = "audited_by_consumer";
    consignment.action.push(action);
    
    const consignmentRegistry = await getAssetRegistry('org.coal_logistics.basic.consignment');
    await consignmentRegistry.update(consignment);
  }
  else{
    throw new Error ("Action is available only to auditors assigned by consumner");
  }
}
