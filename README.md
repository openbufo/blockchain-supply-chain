# blockchain-supply-chain

Supply chain management project using blockchain and IoT.

# Installation

Business Network Archive (.bna) is available inside the hyperledger-fabric directory. It can be deployed in any hyperledger fabric network to replicate the application. 
The application also consists of a web based UI. Distributable files are available inside UI/build_v2. 

# Demo

The application is hosted on the cloud. Use the following link: http://35.200.213.226/

App consists of 4 roles. Use the following credentials for different roles.

 - Miner - userId: miner
           password: miner@123
         
 - Auditor - userId: auditor
            password: auditor@123
          
 - Transporter - userId: transporter
                password: transporter@123
               
 - Customer   - userId: customer
                password: customer@123
               
