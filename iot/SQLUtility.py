from datetime import datetime
import logging
from ConfigParser import SafeConfigParser
import MySQLdb
import json

CONFIGURATION = "setup.conf"

# Initialize Logger
current_date = datetime.now().strftime("%y_%m_%d")
logging.basicConfig(filename='Log/SQLuploader' + current_date + '.log', level=logging.DEBUG)
logger = logging

class SQL():
    def __init__(self):
        try:
            parser = SafeConfigParser()
            parser.read(CONFIGURATION)
            self.host = parser.get("sqlSettings", "host")
            self.user = parser.get("sqlSettings", "user")
            self.password = parser.get("sqlSettings", "password")
            self.database = parser.get("sqlSettings", "database")
            self.table = parser.get("sqlSettings", "table")
            self.fieldMappingFile = parser.get("sqlSettings", "mapping")
            self.db = MySQLdb.connect(self.host, self.user, self.password, self.database)
            self.cursor = self.db.cursor()
            logger.info("connected to database")

        except Exception as e:
            logging.error("Exception in connecting database : " + str(e))

        try:
            # read field_name : mqtt_key mapping json
            with open(self.fieldMappingFile, "r") as f:
                self.field_mapping = json.load(f)
        except Exception as e:
            logging.error("Exception in reading field mapping : " + str(e))

    # Generate SQL query and insert into database
    def insert_into_db(self, table, data):
        try:
            keyStr = ''
            valueStr = ''
            for field, val in data.iteritems():
                try:
                    keyStr += '`' + str(self.field_mapping[field]) + '`,'
                    valueStr += "'" + str(val) + "',"
                except Exception as e1:
                    logging.debug("Error in SQL query formation :" + str(e1))
                    pass
            SQLCommand = "INSERT INTO " + str(table) + ' (' + str(keyStr).rstrip(',') + ") VALUES (" + str(valueStr).rstrip(',') + ");"
            self.cursor.execute(SQLCommand)
            self.db.commit()
        except Exception as error:
            logging.info("SQL Command : " + str(SQLCommand))
            logging.error("Exception in insert into DB: " + str(error))


    # prepares data
    def upload(self, data):
        try:
            tempJson = data['values']
            timestamp = data["timeStamp"]
            stationId = data["stationId"]
            subStationId = data["subStationId"]
            datetime_str = datetime.fromtimestamp(int(timestamp)).strftime('%Y-%m-%d %H:%M:%S')
            tempJson.update({"timestamp": timestamp, "date_time": datetime_str, "stationId": stationId, "subStationId": subStationId})

            # Form the SQL query and insert into database
            self.insert_into_db(self.table, tempJson)
            return True
        except Exception as e1:
            logging.error("Exception in generate data : " + str(e1))
            return False