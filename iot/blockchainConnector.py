# AWS Lambda function to push data into the blockchain
# Function is triggered by AWS IoT when new message is received
# uses REST API to pushes data into the hyperledger network

import json
import os
import urllib
from urllib import request, parse

API_BASE_URL = "http://35.200.213.226:3000/api/{}"
HYPERLEDGER_ACTION = os.environ.get("HYPERLEDGER_ACTION") 		# currently supports inspectQualityByConsumer, inspectQualityByMiner and updateWeight
HYPERLEDGER_ACCOUNT_ID = os.environ.get("HYPERLEDGER_ACCOUNT_ID")
HYPERLEDGER_AUTH_TOKEN = os.environ.get("HYPERLEDGER_AUTH_TOKEN")
CONSIGNMENT_ID = os.environ.get("CONSIGNMENT_ID")

def lambda_handler(event, context):
	stationId = event['stationId']
	stationName = event['stationName']
	payload = event['values']
	timestamp = event['timeStamp']
    
    if not HYPERLEDGER_ACCOUNT_ID:
        return "Unable to access hyperledger network ID."
    elif not HYPERLEDGER_AUTH_TOKEN:
        return "Unable to access hyperledger auth token."
    elif not HYPERLEDGER_ACTION or HYPERLEDGER_ACTION not in ["inspectQualityByMiner", "inspectQualityByConsumer"]:
        return "The function needs an action"

    currentTime = datetime.datetime.now().replace(microsecond=0).isoformat()

    if HYPERLEDGER_ACTION is "updateWeight":
    	latitude = payload['latitude']
    	longitude = payload['longitude']
    	weight = payload['weight']
    	post_params = {
			  "$class": "org.coal_logistics.basic.updateWeight",
			  "Consignment": "resource:org.coal_logistics.basic.consignment#" + str(CONSIGNMENT_ID),
			  "measuredWeight": {
			    "$class": "org.coal_logistics.basic.weight",
			    "value": weight,
			    "unit": "tons",
			    "fetchTime": currentTime,
			    "weighLocation": {
			      "$class": "org.coal_logistics.basic.location",
			      "latitude": longitude,
			      "longitude": longitude,
			      "name": stationName,
			      "fetchTime": currentTime
			    }
			  }
			}
	elif HYPERLEDGER_ACTION is in ["inspectQualityByMiner", "inspectQualityByConsumer"]:
		ash = payload['ash']
		moisture = payload['moisture']
		volatility = payload['volatility']
		FC = payload['volatility']
		gcv = payload['gcv']
		grade =payload['grade']
		post_params = {
			  "$class": "org.coal_logistics.basic.inspectQualityByMiner",
			  "Consignment": "resource:org.coal_logistics.basic.consignment#" + str(CONSIGNMENT_ID),
			  "Auditor": "resource:org.coal_logistics.basic.auditor#" + str(stationId),
			  "Quality": {
			    "$class": "org.coal_logistics.basic.quality",
			    "ash": ash,
			    "moisture": moisture,
			    "volatility": volatility,
			    "FC": FC,
			    "gcv": gcv,
			    "grade": grade,
			    "auditedBy": "resource:org.coal_logistics.basic.auditor" + str(stationId),
			    "auditTime": currentTime
			  }
			}

    populated_url = API_BASE_URL.format(HYPERLEDGER_ACTION)
    
    # encode the parameters for Python's urllib
    data = parse.urlencode(post_params).encode()
    req = request.Request(populated_url)

    # add authentication header to request based on Account SID + Auth Token
    authentication = "{}:{}".format(HYPERLEDGER_ACCOUNT_ID, HYPERLEDGER_AUTH_TOKEN)
    base64string = base64.b64encode(authentication.encode('utf-8'))
    req.add_header("Authorization", "Basic %s" % base64string.decode('ascii'))

    try:
        # perform HTTP POST request
        with request.urlopen(req, data) as f:
            print("Hyperledger returned {}".format(str(f.read().decode('utf-8'))))
    except Exception as e:
        # something went wrong!
        return e

    return "Data pushed to hyperledger network successfully!"