from pymodbus.client.sync import ModbusSerialClient as ModbusSERIALClient
from pymodbus.client.sync import ModbusTcpClient
import time
import sys
import serial
import argparse
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.constants import Endian


def start(client, registertype, startAddress, totalRegister, unitId):
    while True:
        if registertype == "HR" or registertype == "hr":
            try:
                connect_status = client.connect()
                response = client.read_holding_registers(int(startAddress), int(totalRegister), unit=int(unitId))
                print "raw response: " + str(response)
                print "registers: "+str(response.registers)
                simpleFloat(response.registers)
                swappedFloat(response.registers)
                swappedLongInteger(response.registers)
                swappedDouble(response.registers)
                simpleDouble(response.registers)
                LongInteger(response.registers)
            except Exception as e:
                print "Some error Occured:" + str(e)
                print e.message
        elif registertype == "IR" or registertype == "ir":
            try:
                connect_status = client.connect()
                response = client.read_input_registers(int(startAddress), int(totalRegister), unit=int(unitId))
                print "raw response: " + str(response)
                print "registers: "+str(response.registers)
                simpleFloat(response.registers)
                swappedFloat(response.registers)
                swappedLongInteger(response.registers)
                LongInteger(response.registers)
            except Exception as e:
                print "Some error Occured:" + str(e)
                print e.message
        elif registertype == "CS" or registertype == "cs":
            try:
                connect_status = client.connect()
                response = client.read_coils(int(startAddress), int(totalRegister), unit=int(unitId))
                print "raw response: " + str(response)
                print "registers: "+str(response.bits)
            except Exception as e:
                print "Some error Occured:" + str(e)
                print e.message
        elif registertype == "is" or registertype == "is":
            try:
                connect_status = client.connect()
                response = client.read_discrete_inputs(int(startAddress), int(totalRegister), unit=int(unitId))
                print "raw response: " + str(response)
                print "registers: "+str(response.bits)
            except Exception as e:
                print "Some error Occured:" + str(e)
                print e.message
        print "\n"
        time.sleep(5)


def simpleFloat(response):
    floatReading = []
    try:
        length = len(response)/2
        for channelNo in range(0, length):
            icount = channelNo * 2
            temp_data=[]
            temp_data.append(response[icount])
            temp_data.append(response[icount+1])
            decoder = BinaryPayloadDecoder.fromRegisters(temp_data, endian=Endian.Big)
            value = decoder.decode_32bit_float()
            floatReading.append(value)
        print "Float: "+str(floatReading)
    except Exception as e:
        print e
        print e.message


def swappedFloat(response):
    floatReading = []
    try:
        length = len(response)/2
        for channelNo in range(0, length):
            icount = channelNo * 2
            temp_data=[]
            temp_data.append(response[icount+1])
            temp_data.append(response[icount])
            decoder = BinaryPayloadDecoder.fromRegisters(temp_data, endian=Endian.Big)
            value = decoder.decode_32bit_float()
            floatReading.append(value)
        print "Swapped Float: "+ str(floatReading)
    except Exception as e:
        print e
        print e.messages


def simpleDouble(response):
    reading = []
    try:
        length = len(response) / 4
        for channelNo in range(0, length):
            icount = channelNo * 4
            temp_data = []
            temp_data.append(response[icount])
            temp_data.append(response[icount + 1])
            temp_data.append(response[icount + 2])
            temp_data.append(response[icount + 3])
            decoder = BinaryPayloadDecoder.fromRegisters(temp_data, endian=Endian.Big)
            value = decoder.decode_64bit_float()
            reading.append(value)
        print "Double: ", reading
    except Exception as e:
        print e
        print e.message


def swappedDouble(response):
    reading = []
    try:
        length = len(response) / 4
        for channelNo in range(0, length):
            icount = channelNo * 4
            temp_data = []
            temp_data.append(response[icount + 3])
            temp_data.append(response[icount + 2])
            temp_data.append(response[icount + 1])
            temp_data.append(response[icount])
            decoder = BinaryPayloadDecoder.fromRegisters(temp_data, endian=Endian.Big)
            value = decoder.decode_64bit_float()
            reading.append(value)
        print "Swapped Double: ", reading
    except Exception as e:
        print e
        print e.message


def swappedLongInteger(response):
    longReading = []
    try:
        length = len(response)/2
        for channelNo in range(0, length):
            icount = channelNo * 2
            temp_data=[]
            temp_data.append(response[icount+1])
            temp_data.append(response[icount])
            decoder = BinaryPayloadDecoder.fromRegisters(temp_data, endian=Endian.Big)
            value = decoder.decode_32bit_int()
            longReading.append(value)
        print "Long Integer Swapped: "+ str(longReading)
    except Exception as e:
        print e
        print e.messages


def LongInteger(response):
    longReading = []
    try:
        length = len(response)/2
        for channelNo in range(0, length):
            icount = channelNo * 2
            temp_data=[]
            temp_data.append(response[icount])
            temp_data.append(response[icount+1])
            decoder = BinaryPayloadDecoder.fromRegisters(temp_data, endian=Endian.Big)
            value = decoder.decode_32bit_int()
            longReading.append(value)
        print "Long Integer : "+ str(longReading)
    except Exception as e:
        print e
        print e.messages

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
    description='Python Modbus Tester',
    epilog="""Please provide the input as per above input""")
    parser.add_argument('connectionType', type=str, default="rtu", help='rtu')
    parser.add_argument('com_port/IP', type=str, default="/dev/ttyUSB0", help='COM1')
    parser.add_argument('registerType', type=str, default="HR", help='HR/IR/CS/IS')
    parser.add_argument('unitId', type=int, default=1, help='1')
    parser.add_argument('startAddress', type=int, default=1, help='1')
    parser.add_argument('totalRegister', type=int, default=1, help='1')
    parser.add_argument('baud_rate/port', type=int, default=9600, help='1')
    args = parser.parse_args()

    print "Using the following configuration:\n"
    print "args: " + str(args)


    try:
        if sys.argv[1].lower() == "rtu":
            print "calling rtu client"
            client = ModbusSERIALClient(method="rtu", port=sys.argv[2], stopbits=1, bytesize=8, parity=serial.PARITY_NONE,
                                        baudrate=int(sys.argv[7]))
            start(client, sys.argv[3], sys.argv[5], sys.argv[6], sys.argv[4])

        elif sys.argv[1].lower() == "tcp":
            print "Calling tcp client"
            client = ModbusTcpClient(host=sys.argv[2], port=int(sys.argv[7]))
            start(client, sys.argv[3], sys.argv[5], sys.argv[6], sys.argv[4])

        else:
            raise Exception("Invalid Arguments")
    except Exception as e:
        print e