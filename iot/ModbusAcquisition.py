# -*- coding: utf-8 -*-
import time
import copy
from datetime import datetime
import traceback
from Logging import logger
from ConfigParser import SafeConfigParser
from pymodbus.client.sync import ModbusSerialClient as ModbusSERIALClient
from pymodbus.payload import BinaryPayloadDecoder
from pymodbus.constants import Endian
from pymodbus.client.sync import ModbusTcpClient
import SQLUtility
import serial

global sleepTime, uploadData, fileUploaderObj

class Monitor:

    def __init__(self):
        print ("Inside Init")
        self.dataJson = {}
        self.dataJson["collectors"] = {}
        self.readConfig()
        self.uploafFlag = 0
        self.finalDataJson = {}
        self.previousDataJson = ""
        self.connectivity = False
        self.uploadHistorical = False

    def _decode_list(self, data):
        rv = []
        for item in data:
            if isinstance(item, unicode):
                item = item.encode('utf-8')
            elif isinstance(item, list):
                item = self._decode_list(item)
            elif isinstance(item, dict):
                item = self._decode_dict(item)
            rv.append(item)
        return rv

    def _decode_dict(self, data):
        rv = {}
        for key, value in data.iteritems():
            if isinstance(key, unicode):
                key = key.encode('utf-8')
            if isinstance(value, unicode):
                value = value.encode('utf-8')
            elif isinstance(value, list):
                value = self._decode_list(value)
            elif isinstance(value, dict):
                value = self._decode_dict(value)
            rv[key] = value
        return rv

    def compareJson(self, jsonA, jsonB):
        try:
            for everyElement in jsonA:
                if jsonA[everyElement] != jsonB[everyElement]:
                    return False
            return True
        except:
            return False

    def readConfig(self):
        parser = SafeConfigParser()
        global sleepTime, uploadData
        parser.read("setup.conf")
        try:
            self.totalCollector = int(parser.get("siteSettings", "totalCollectors"))
            self.totalParamteters = int(parser.get("siteSettings", "totalParameters"))
            self.baudrate = parser.get("siteSettings", "baudrate")
            self.comport = parser.get("siteSettings", "comport")
            self.compare = int(parser.get("siteSettings", "compare"))
            sleepTime = int(parser.get("siteSettings", "sleepTime"))
            uploadData = int(parser.get("siteSettings", "uploadData"))
        except Exception as e:
            traceback.print_exc()
            logger.debug("Exception in reading siteSettings")
            logger.exception(e)

        try:
            self.stationId = parser.get("siteDetails", "stationId")
            self.subStationId = parser.get("siteDetails", "subStationId")
            self.siteLocation = parser.get("siteDetails", "siteLocation")
            self.dataJson["stationId"] = self.stationId
            self.dataJson["siteLocation"] = self.siteLocation
        except Exception as e:
            traceback.print_exc()
            logger.debug("Exception in reading siteDetails")
            logger.exception(e)

        for i in range(1, self.totalCollector + 1):
            try:
                tempCollectorId = parser.get("Collector"+str(i), "CollectorId")
                tempCollectorType = parser.get("Collector"+str(i), "CollectorType")
                deviceIp = parser.get("Collector"+str(i), "deviceIp")
                start = parser.get("Collector"+str(i), "start")
                proxy = parser.get("Collector"+str(i), "proxy")
                comPort = parser.get("Collector"+str(i), "comPort")
                deviceId = parser.get("Collector"+str(i), "deviceId")
                totalRegister = parser.get("Collector"+str(i), "totalRegister")
                baudRate = parser.get("Collector"+str(i), "baudRate")
                self.dataJson["collectors"][tempCollectorId] = {}
                self.dataJson["collectors"][tempCollectorId]["CollectorId"] = tempCollectorId
                self.dataJson["collectors"][tempCollectorId]["CollectorType"] = tempCollectorType
                self.dataJson["collectors"][tempCollectorId]["deviceIp"] = deviceIp
                self.dataJson["collectors"][tempCollectorId]["start"] = start
                self.dataJson["collectors"][tempCollectorId]["comPort"] = comPort
                self.dataJson["collectors"][tempCollectorId]["deviceId"] = deviceId
                self.dataJson["collectors"][tempCollectorId]["proxy"] = proxy
                self.dataJson["collectors"][tempCollectorId]["totalRegister"] = totalRegister
                self.dataJson["collectors"][tempCollectorId]["baudRate"] = baudRate
                self.dataJson["collectors"][tempCollectorId]["parameters"] = {}
            except Exception as e:
                traceback.print_exc()
                logger.exception(e)

        for i in range(1, self.totalParamteters + 1):
            try:

                tempParameterId = parser.get("Parameter"+str(i), "parameterId")
                tempParameterName = parser.get("Parameter"+str(i), "parameterName")
                dataType = parser.get("Parameter"+str(i), "dataType")
                channelNo = parser.get("Parameter"+str(i), "channelNo")
                collector = parser.get("Parameter"+str(i), "collector")
                coeffA = parser.get("Parameter"+str(i), "coeffA")
                coeffB = parser.get("Parameter"+str(i), "coeffB")
                self.dataJson["collectors"][collector]["parameters"]["parameter"+str(i)] = {}
                self.dataJson["collectors"][collector]["parameters"]["parameter"+str(i)]["parameterId"] = tempParameterId
                self.dataJson["collectors"][collector]["parameters"]["parameter"+str(i)]["parameterName"] = tempParameterName
                self.dataJson["collectors"][collector]["parameters"]["parameter"+str(i)]["dataType"] = dataType
                self.dataJson["collectors"][collector]["parameters"]["parameter"+str(i)]["channelNo"] = channelNo
                self.dataJson["collectors"][collector]["parameters"]["parameter"+str(i)]["coeffA"] = coeffA
                self.dataJson["collectors"][collector]["parameters"]["parameter"+str(i)]["coeffB"] = coeffB
                self.dataJson["collectors"][collector]["parameters"]["parameter"+str(i)]["value"] = ""

            except Exception as e:
                traceback.print_exc()
                logger.exception(e)
        logger.debug("DataJson: ")
        logger.debug(self.dataJson)

    def simpleFloat(self, response):
        floatReading = []
        try:
            length = len(response) / 2
            for channelNo in range(0, length):
                icount = channelNo * 2
                temp_data = []
                temp_data.append(response[icount])
                temp_data.append(response[icount + 1])
                decoder = BinaryPayloadDecoder.fromRegisters(temp_data, endian=Endian.Big)
                value = decoder.decode_32bit_float()
                floatReading.append(value)
            logger.debug("Float: " + str(floatReading))
            return floatReading
        except Exception as e:
            traceback.print_exc()
            print e
            print e.message

    def swappedFloat(self, response):
        floatReading = []
        try:
            length = len(response) / 2
            for channelNo in range(0, length):
                icount = channelNo * 2
                temp_data = []
                temp_data.append(response[icount + 1])
                temp_data.append(response[icount])
                decoder = BinaryPayloadDecoder.fromRegisters(temp_data, endian=Endian.Big)
                value = decoder.decode_32bit_float()
                floatReading.append(value)
            print "Swapped Float: " + str(floatReading)
            logger.debug("Swapped Float: " + str(floatReading))
            return floatReading
        except Exception as e:
            traceback.print_exc()
            print e
            print e.message

    def longInteger(self, response):
        longIntReading = []
        try:
            length = len(response) / 2
            for channelNo in range(0, length):
                icount = channelNo * 2
                temp_data = []
                temp_data.append(response[icount + 1])
                temp_data.append(response[icount])
                decoder = BinaryPayloadDecoder.fromRegisters(temp_data, endian=Endian.Big)
                value = decoder.decode_32bit_int()
                longIntReading.append(value)
            logger.debug("LongInt: " + str(longIntReading))
            return longIntReading
        except Exception as e:
            traceback.print_exc()
            print e
            print e.message

    def readModbus_HR(self, collector, client):
        data = []
        print "calling hr client"
        try:
            connect_status = client.connect()
            logger.debug("connect_status: " + str(connect_status))
            response = client.read_holding_registers(int(collector["start"]), int(collector["totalRegister"]),
                                                     unit=int(collector["deviceId"]))
            logger.debug("raw response: " + str(response))
            logger.debug("registers: " + str(response.registers))
            data = response.registers
            if "-sf" in str(collector["CollectorType"]).lower():
                data = self.swappedFloat(response.registers)
            elif "-float" in str(collector["CollectorType"]).lower():
                data = self.simpleFloat(response.registers)
            client.close()
        except Exception as e:
            client.close()
            traceback.print_exc()
            print "Some error Occured:" + str(e)
            print e.message
        return data

    def readModbus_IR(self, collector, client):
        data = []
        print "calling rtu client"
        try:
            connect_status = client.connect()
            logger.debug("connect_status: " + str(connect_status))
            response = client.read_input_registers(int(collector["start"]), int(collector["totalRegister"]),
                                                   unit=int(collector["deviceId"]))
            logger.debug("raw response: " + str(response))
            logger.debug("registers: " + str(response.registers))
            data = response.registers
            if "-sf" in str(collector["CollectorType"]).lower():
                data = self.swappedFloat(response.registers)
            elif "-float" in str(collector["CollectorType"]).lower():
                data = self.simpleFloat(response.registers)
            elif "-li" in str(collector["CollectorType"]).lower():
                data = self.longInteger(response.registers)
            client.close()
        except Exception as e:
            client.close()
            traceback.print_exc()
            print "Some error Occured:" + str(e)
            print e.message
        return data

    def readModbus_CS(self, collector, client):
        data = []
        print "calling tcp client"
        try:
            connect_status = client.connect()
            logger.debug("connect_status: " + str(connect_status))
            response = client.read_coils(int(collector["start"]), int(collector["totalRegister"]),
                                         unit=int(collector["deviceId"]))
            logger.debug("raw response: " + str(response))
            logger.debug("registers: " + str(response.bits))
            data = response.bits
            if "-sf" in str(collector["CollectorType"]).lower() or "float" in str(collector["CollectorType"]).lower():
                logger.debug("Float or sFloat is not available for this collector")
            client.close()
        except Exception as e:
            client.close()
            traceback.print_exc()
            print "Some error Occured:" + str(e)
            print e.message
        return data

    def readModbus_IS(self, collector, client):
        data = []
        print "calling rtu client"
        try:
            connect_status = client.connect()
            logger.debug("connect_status: " + str(connect_status))
            response = client.read_discrete_inputs(int(collector["start"]), int(collector["totalRegister"]),
                                                   unit=int(collector["deviceId"]))
            logger.debug("raw response: " + str(response))
            logger.debug("registers: " + str(response.bits))
            data = response.registers
            if "sfloat" in str(collector["CollectorType"]).lower():
                data = self.swappedFloat(response.bits)
            elif "float" in str(collector["CollectorType"]).lower():
                data = self.simpleFloat(response.bits)
            client.close()
        except Exception as e:
            client.close()
            traceback.print_exc()
            print "Some error Occured:" + str(e)
            print e.message
        return data

    def readData(self):
        self.dataJson["timeStamp"] = str(datetime.now()).split(".")[0]
        for everyCollector in self.dataJson["collectors"]:
            collectorType = self.dataJson["collectors"][everyCollector]["CollectorType"]
            deviceIp = self.dataJson["collectors"][everyCollector]["deviceIp"]
            # totalRegister = self.dataJson["collectors"][everyCollector]["totalRegister"]
            # start = self.dataJson["collectors"][everyCollector]["start"]

            client = None
            if "modbus" in collectorType.lower():
                try:
                    collector = self.dataJson["collectors"][everyCollector]
                    if "-rtu" in collectorType.lower():
                        client = ModbusSERIALClient(method="rtu", port=collector["comPort"], stopbits=1, bytesize=8,
                                                    parity=serial.PARITY_ODD, baudrate=int(collector["baudRate"]),timeout=int(50))
                    elif "-tcp" in collectorType.lower():
                        client = ModbusTcpClient(host=deviceIp, port=502)
                    if "-hr" in collectorType.lower():
                        data = self.readModbus_HR(collector, client)
                    elif "-ir" in collectorType.lower():
                        data = self.readModbus_IR(collector, client)
                    elif "-cs" in collectorType.lower():
                        data = self.readModbus_CS(collector, client)
                    elif "-is" in collectorType.lower():
                        data = self.readModbus_IS(collector, client)
                    try:
                        client.close()
                    except Exception as e:
                        print "Exception while closing the connection"
                        logger.exception(e)
                except Exception as e:
                    logger.exception(e)
                    print "Unable to connect to Modbus Server"

            try:

                for everyParameter in self.dataJson["collectors"][everyCollector]["parameters"]:
                    try:
                        dataType = self.dataJson["collectors"][everyCollector]["parameters"][everyParameter]["dataType"]
                        channelData = data[dataType]
                    except Exception as e:
                        #logger.exception(e)
                        channelData = data
                    coeffA = self.dataJson["collectors"][everyCollector]["parameters"][everyParameter]["coeffA"]
                    coeffB = self.dataJson["collectors"][everyCollector]["parameters"][everyParameter]["coeffB"]
                    channelno = self.dataJson["collectors"][everyCollector]["parameters"][everyParameter]["channelNo"]
                    parameterId = self.dataJson["collectors"][everyCollector]["parameters"][everyParameter]["parameterId"]

                    try:
                        if coeffA == "" and coeffB == "":
                            if "-bit" in collectorType.lower():
                                bitdata = str(bin(channelData[int(str(channelno).split("-")[0])])[2:]).zfill(16)[::-1]
                                self.dataJson["collectors"][everyCollector]["parameters"][everyParameter]["value"] = bitdata[int(str(channelno).split("-")[1])]
                            else:
                                self.dataJson["collectors"][everyCollector]["parameters"][everyParameter]["value"] = str(round(channelData[int(channelno)],2))
                        else:
                            tempValue = float(channelData[int(channelno)])
                            tempValue = tempValue * float(coeffA)
                            tempValue = round(tempValue + float(coeffB), 2)
                            #tempValue = float("{0:.2f}".format(tempValue))
                            # print tempValue
                            self.dataJson["collectors"][everyCollector]["parameters"][everyParameter]["value"] = str(tempValue)
                        self.finalDataJson[parameterId] = \
                        self.dataJson["collectors"][everyCollector]["parameters"][everyParameter]["value"]
                    except Exception as e:
                        traceback.print_exc()
                        logger.debug("invalid channel number for parameter: ")
            except Exception as e:
                print e.message
                traceback.print_exc()
                logger.exception(e)
        logger.debug("FinalData: ")
        logger.debug(self.dataJson)

    def uploadData(self):
        try:
            finalData = {}
            finalData["stationId"] = self.stationId
            finalData["subStationId"] = self.subStationId
            finalData["data"] = self.finalDataJson
            finalData["timeStamp"] = int(time.mktime(datetime.now().timetuple()))

            if self.compare == 1:
                if not self.compareJson(finalData["data"], self.previousDataJson):

                    try:
                        tempDataJson = copy.deepcopy(finalData["data"])
                        for everyItem in tempDataJson:
                            if self.previousDataJson != '' and finalData["data"][everyItem] == self.previousDataJson[everyItem]:
                                finalData["data"].pop(everyItem)
                    except Exception as e:
                        traceback.print_exc()
                        print e
                        pass

                    # ################### Write into SQL #######################
                    sql.upload(finalData)

            else:
                tempData = {}
                tempData["values"] = self.finalDataJson
                tempData["stationId"] = self.stationId
                tempData["subStationId"] = self.subStationId
                tempData["timeStamp"] = int(time.mktime(datetime.now().timetuple()))

                # ################### Write into SQL #######################
                sql.upload(finalData)

        except Exception as e:
            traceback.print_exc()
            logger.exception(e)
        finally:
            pass


Object = Monitor()
sql = SQLUtility.SQL()
while True:
    try:
        global sleepTime, uploadData
        Object.readData()
        if uploadData == 1:
            Object.uploadData()
    except Exception as e:
        traceback.print_exc()
        print e.message
        logger.exception(e)

    # set timer
    time.sleep(int(sleepTime))

