# -*- coding: utf-8 -*-
# script uploads data to AWS IoT using MQTT
# to be executed after regular interval using cronjob for historical uploads

from ConfigParser import SafeConfigParser
from Logging import logger
import paho.mqtt.client as mqtt
import datetime
import traceback
import json
import glob
import os
import time

class uploadData:
    def __init__(self):
        self.readConfig()
        self.connectServer()

    def readConfig(self):
        parser = SafeConfigParser()
        parser.read("setup.conf")
        try:
            self.MqttIp = parser.get("siteSettings", "AWSHost")
            self.MqttPort = parser.get("siteSettings", "AWSPort")
            self.topic = parser.get("siteSettings", "topic")
            self.cert = parser.get("siteSettings", "certificate")
            self.key = parser.get("siteSettings", "key")
            self.sleepTime = parser.get("siteSettings", "sleeptime")
        except Exception as e:
            traceback.print_exc()
            logger.debug("Exception in reading siteSettings")
            logger.exception(e)

    def connectServer(self):
        self.mqttc = mqtt.Client("python_pub")
        self.mqttc.tls_set(certifi.where(),
              certfile=self.cert,
              keyfile=self.key,
              cert_reqs=ssl.CERT_REQUIRED,
              tls_version=ssl.PROTOCOL_TLSv1_2,
              ciphers=None)
        try:
            #self.mqttc.username_pw_set()
            self.connectionFailed = self.mqttc.connect(self.MqttIp, int(self.MqttPort))
            return True
        except Exception as e:
            print "Unable to connect to the server trying to reconnect. "
            logger.exception(e)
            traceback.print_exc()
            self.connectionFailed = 1
            return False

    def awsIoTUploader(self, message):
        try:
            MQTTresponse = self.mqttc.publish(self.topic, str(message))
            if not MQTTresponse.is_published():
                self.mqttc.reconnect()
                MQTTresponse = self.mqttc.publish(self.topic, str(message))
                self.mqttc.loop(2)  # timeout = 2s
            if MQTTresponse:
                return True
            return False
        except Exception as e:
            logger.exception(e)

    def writeintoJSON(self, data):
        try:
            # create a new file
            print "Creating new json file in backup"
            filename = "backup/" + str(datetime.datetime.now().strftime("%Y%m%d%H%M%S")) + ".json"
            jsonFileObj = open(filename, "wb")
            json.dump(data, jsonFileObj)
        except Exception as e:
            print "Something went wrong while writing json"
            print e

    def uploadfromJson(self):
        # open all files in backup directory, individually
        for filename in glob.glob(os.path.join("backup", '*.json')):
            try:
                filepath = str(filename)
                if os.stat(filepath).st_size==0:
                    #print "deleting file " + str(filepath)
                    os.remove(filepath)
                else:
                    jsonFileObj = open(filepath, "r+")
                    jsonDumpData = json.load(jsonFileObj)
                    jsonFileObj.close()
                    MQTTresponse = self.awsIoTUploader(str(jsonDumpData))
                    if MQTTresponse:
                        # delete the file
                        print "deleting file " + str(filepath)
                        os.remove(filepath)
                        logger.debug("Pushed via MQTT")
                        logger.info(jsonDumpData)
            except Exception as e:
                logger.exception(e)
                continue
        return True

    def send(self, message):
        try:
            res = self.awsIoTUploader(str(message))
            if not res:
                # create a json backup of the data
                data = json.loads(message)  # convert string to json
                writeintoJSON(data)
                return True
            else:
                print "Failed to connect to mqtt"
                logger.debug("Failed to connect to the mqtt server")
                return False
        except Exception as e:
            logger.exception(e)

if __name__ == "__main__":
    try:
        uploadObj = uploadData()
        if uploadObj.uploadfromJson():
            logger.info("All backup data uploaded successfully")

        #if uploadObj.uploadMqtt():
            #print "Data uploaded via MQTT successfully"
    except Exception as e:
        logger.exception(e)
    finally:
        time.sleep(int(uploadObj.sleepTime))
