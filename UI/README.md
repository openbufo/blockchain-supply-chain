# Blockchain-supplyChain

This is the UI code for supplyChain

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4900/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build --prod --aot=false` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

