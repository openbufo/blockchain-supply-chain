import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Http, HttpModule } from '@angular/http';
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import {MdProgressBarModule} from '@angular2-material/progress-bar';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuardService } from './services/auth-guard.service';
import {Logger, Options} from "angular2-logger/core"

// import { AgmCoreModule } from 'angular2-google-maps/core';

@NgModule({
  declarations: [
    AppComponent    
    
    
  ],
  imports: [
    BrowserModule,
    MdProgressBarModule,
    // BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
  
    //NoopAnimationsModule
    // AgmCoreModule.forRoot({
    //   // apiKey: 'AIzaSyAkp4aUFjgGeiAwEdwI8vi3w7q4JmfrQes'
    //   apiKey: 'AIzaSyBWDe8J0eBajWGjpdgZQplptgN733g2JgQ'
    // })
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy}, 
    AuthGuardService, 
    Logger,
    Options
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
