import {  Directive, ElementRef, Input, AfterViewInit,Renderer } from '@angular/core';
import { StorageService} from '../services/storage.service';

@Directive({
  selector: '[appPermission]',
  providers: [StorageService]
})
export class PermissionDirective implements AfterViewInit {
  @Input('appPermission') config: string;
  public permissionInfo;

  constructor(
    public el: ElementRef, 
    public renderer: Renderer,
    public _storage: StorageService
  ) {}

  permissions = { 
    "mfg-user":{
       "current-product-level":true,
       "product-distribution":true,
       "distributor-performance":true,
       "alerts_notifications":true
     },
    "dispenser_user":{
       "alerts_notifications":true,
       "all_verifications":true,
       "incoming_shipments":true,
     },
    "distributor_user":{
       "current-product-level":true,
       "product-distribution":true,
       "ship_verify":true,
       "alerts_notifications":true
     }
   }
  ngAfterViewInit(): void {
//     try {
//       let configTemp = this.config.split(',');
//       //   this.permissionInfo = this._storage.api.local.get('token')['permissions'];
//       this.permissionInfo = this.permissions
//       console.log("permission info is ",this.permissionInfo);
//       if(this.permissionInfo[configTemp])
//     if(this.permissionInfo[configTemp[0]]['access'].length == 1 && this.permissionInfo[configTemp[0]]['access'].indexOf('VIEW') > -1){
//         this.renderer.setElementStyle(this.el.nativeElement, 'display', 'none');
//     }

//     if(config)
      
//     } catch (error) {
//       console.log(error);
//     }
//   }	  
}
}
