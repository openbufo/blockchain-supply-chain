// declare module "../../assets/jsons/config.json" {
//     const value: any;
//     export default value;
// }
// import * as data from '../../assets/jsons/config.json';

const currentDomain = '';
// const currentDomain = 'http://' + window.location.hostname;
// const currentDomain = 'https://' + window.location.hostname;

const API_END_POINT = 'http://35.200.213.226:3000/api/';

export class AppSettings {
    public static SESSION_KEY = {
        TOKEN: 'token'
    };

    public static URL:any = {
        //====================== Dashboard ===================================
        // DATA_RECORD_LIST: LOCAL_END_POINT_STATUS+'record_count',
        //====================== Dashboard URLS ==============================
            DASHBOARD : API_END_POINT+'dashboard',
            CURRENT_PRODUCT_LEVEL: API_END_POINT+'assets/jsons/current_product_level',
            PRODUCT_DISTRIBUTION: API_END_POINT+'assets/jsons/product_distribution',
            DISTRIBUTION_PERFORMANCE: API_END_POINT+'assets/jsons/distribution_performance',
            ALERTS_NOTIFICATIONS: API_END_POINT+'assets/jsons/alerts_notifications',
            ALL_VERIFICATIONS : API_END_POINT+'assets/jsons/view_verifications',
            INCOMING_SHIPMENTS: API_END_POINT+'assets/jsons/incoming_shipments',

        //======================================================================
            VIEW_VERIFICATIONS:API_END_POINT+'assets/jsons/view_verifications',
            PRODUCTS: API_END_POINT+'products',
            NOTIFICATIONS_LIST:API_END_POINT+'assets/jsons/notifications_list',
            NOTIFICATION_DETAIL:API_END_POINT+'assets/jsons/notification_details',

        //======================================================================
            ACKNOWLEDGE_CONSIGNMENT:API_END_POINT+'',
            SHIP_CONSIGNMENT:API_END_POINT+'',
            RECEIVE_CONSIGNMENT:API_END_POINT+'',
            VALIDATE_CONSIGNMENT:API_END_POINT+'',
            DISPATCH_CONSIGNMENT:API_END_POINT+'',
            CREATE_CONSIGNMENT:API_END_POINT+'createConsignment',

            GET_TRANSACTION_DETAILS:API_END_POINT+'assets/jsons/transaction_details.json',
            //GET_CURRENT_TRANSACTIONS:API_END_POINT+'assets/jsons/current_transactions.json'
            GET_CURRENT_TRANSACTIONS:API_END_POINT+'consignment'
    }
} 

/*used in util.service */
export const DATE_FORMATS = {
    dashes: 1
};