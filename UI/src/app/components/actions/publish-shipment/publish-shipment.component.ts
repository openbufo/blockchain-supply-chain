import { Component, OnInit,Output, Input,EventEmitter} from '@angular/core';
import { Router,
  ActivatedRoute,
  NavigationEnd,
  Event as RouterEvent} from '@angular/router';

/**
 * The possible reasons a modal has been closed.
 */
export enum ModalAction { POSITIVE, CANCEL }
/**
 * Models the result of closing a modal dialog.
 */
export interface ModalResult {
  action: ModalAction;
}

@Component({
  selector: 'app-publish-shipment',
  templateUrl: './publish-shipment.component.html',
  styleUrls: ['./publish-shipment.component.css']
})
export class PublishShipmentComponent implements OnInit {
 /**
   * Fires an event when the modal is closed. The argument indicated how it was closed.
   * @type {EventEmitter<ModalResult>}
   */
  @Output('closed') closeEmitter: EventEmitter<ModalResult> = new EventEmitter<ModalResult>();
  /**
  * Fires an event when the modal is ready with a pointer to the modal.
  * @type {EventEmitter<Modal>}
  */
 @Output('loaded') loadedEmitter: EventEmitter<PublishShipmentComponent> = new EventEmitter<PublishShipmentComponent>();

 @Output() selectedData :EventEmitter<any>= new EventEmitter<any>();

 @Input() showModal: boolean = false;
 verified: boolean = false;

 connectionList=[
   {
    "label":"Load Excel File",
    "key":"load_excel_file"
   },
   {
    "label":"SAP ATTP Connection",
    "key":"sap_attp_connection"
  }
 ]
constructor( private _router:Router,
  private _routeObj: ActivatedRoute,) {
    _router.events.subscribe((event: RouterEvent) => {
      var arr=[]
      if(event instanceof NavigationEnd){
         this.showModal= true;
      }
   });
}

ngOnInit() {
}
 cancelAction() {
   this.showModal = false;
   this._router.navigate(['landing/dashboard']);
   this.closeEmitter.next({
       action: ModalAction.CANCEL
   });
   return false;
 }

 product_properties = [
  {
    "key":"Product Name",
    "value":"Product 1"
  },
  {
    "key":"GTIN/Serial_Number",
    "value":"003003183912922938339"
  },
  {
    "key":"Lot_Number",
    "value":"A10011"
  },
  {
    "key":"Expiration Date",
    "value":"2105311"
  },
        {
    "key":"Expiration Date",
    "value":"2105311"
  }
 ]

 selectedConnection;
 connectionSelected=false;
 shipped = false;
 continueAction(){
   if(this.shipped){
    this._router.navigate(['landing/dashboard']);
   }else if(!this.connectionSelected && this.selectedConnection ){
    this.connectionSelected=true;
   }else if(this.connectionSelected && this.selectedConnection){
    this.shipped=true;
   }
 }
}
