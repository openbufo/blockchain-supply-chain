import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishShipmentComponent } from './publish-shipment.component';

describe('PublishShipmentComponent', () => {
  let component: PublishShipmentComponent;
  let fixture: ComponentFixture<PublishShipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishShipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishShipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
