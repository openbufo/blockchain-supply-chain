import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ViewVerificationsComponent} from '../view-verifications/view-verifications.component';
import { ActionsComponent } from '../actions.component';
import { VerifyProductComponent} from '../verify-product/verify-product.component';
import { PublishShipmentComponent} from '../publish-shipment/publish-shipment.component';

/** coal components */
import { AcknowledgepConsignmentComponent } from '../acknowledgep-consignment/acknowledgep-consignment.component';
import { ValidateConsignmentComponent } from '../validate-consignment/validate-consignment.component';
import { CreateConsignmentComponent } from '../create-consignment/create-consignment.component';
import { DispatchConsignmentmentComponent } from '../dispatch-consignmentment/dispatch-consignmentment.component';
import { ReceiveConsignmentComponent } from '../receive-consignment/receive-consignment.component';
import { ShipConsignmentComponent } from '../ship-consignment/ship-consignment.component';

const routes : Routes = [
  { 
    path: '', component: ActionsComponent,
    children: [
      {
        path: 'actions',
        component: ActionsComponent
      },
      {
        path:'acknowledge-consignment',
        component:AcknowledgepConsignmentComponent
      },
      {
        path:'validate-consignment',
        component:ValidateConsignmentComponent
      },
      {
        path:'create-consignment',
        component:CreateConsignmentComponent
      },
      {
        path:'dispatch-consignment',
        component:DispatchConsignmentmentComponent
      },
      {
        path:'receive-consignment',
        component:ReceiveConsignmentComponent
      },
      {
        path:'ship-consignment',
        component:ShipConsignmentComponent
      },
      {
        path:'view-verifications',
        component:ViewVerificationsComponent
      },
      {
        path:'verify-product',
        component:VerifyProductComponent
      },
      {
        path:'publish-shipment',
        component:PublishShipmentComponent
      }
    ]
  } 
];
 
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [ RouterModule ]
})
export class ActionsRoutingModule { }
