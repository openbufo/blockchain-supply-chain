import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiveConsignmentComponent } from './receive-consignment.component';

describe('ReceiveConsignmentComponent', () => {
  let component: ReceiveConsignmentComponent;
  let fixture: ComponentFixture<ReceiveConsignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiveConsignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiveConsignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
