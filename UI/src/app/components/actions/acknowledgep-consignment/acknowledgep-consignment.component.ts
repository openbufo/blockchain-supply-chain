import { Component, OnInit,Output, Input,EventEmitter} from '@angular/core';
import { Router,
  ActivatedRoute,
  NavigationEnd,
  Event as RouterEvent} from '@angular/router';
import { CommonHttpService } from '../../../services/comman-http-request.service'
import { StorageService } from '../../../services/storage.service';
import { AppSettings } from '../../../configuration/AppSettings';

/**
 * The possible reasons a modal has been closed.
 */
export enum ModalAction { POSITIVE, CANCEL }
/**
 * Models the result of closing a modal dialog.
 */
export interface ModalResult {
  action: ModalAction;
}

@Component({
  selector: 'app-acknowledgep-consignment',
  templateUrl: './acknowledgep-consignment.component.html',
  styleUrls: ['./acknowledgep-consignment.component.css']
})
export class AcknowledgepConsignmentComponent implements OnInit {
/**
   * Fires an event when the modal is closed. The argument indicated how it was closed.
   * @type {EventEmitter<ModalResult>}
   */
  @Output('closed') closeEmitter: EventEmitter<ModalResult> = new EventEmitter<ModalResult>();
  /**
  * Fires an event when the modal is ready with a pointer to the modal.
  * @type {EventEmitter<Modal>}
  */
 @Output('loaded') loadedEmitter: EventEmitter<AcknowledgepConsignmentComponent> = new EventEmitter<AcknowledgepConsignmentComponent>();

 @Output() selectedData :EventEmitter<any>= new EventEmitter<any>();

 @Input() showModal: boolean = false;
 verified: boolean = false;
 @Input('title') title:string;
 @Input('rangeUnits') rangeUnits; 
 @Input('rangeCommon') rangeCommon;
public colorVal; 
public colorRule;
public colorUnit;
public acknowledge_data={ orgId:"", consignmentId:"" }

constructor( private _router:Router,public _storage: StorageService,
  private _routeObj: ActivatedRoute,public _commonHttpService:CommonHttpService) {
    _router.events.subscribe((event: RouterEvent) => {
      var arr=[]
      if(event instanceof NavigationEnd){
         this.showModal= true;
      }
   });
}

userType
 ngOnInit() {
  let userDetails = this._storage.api.local.get("currentUser");
  this.userType = userDetails.principal_role;
 }
 cancelAction() {
   this.showModal = false;
   this._router.navigate(['landing/dashboard']);
   this.closeEmitter.next({
       action: ModalAction.CANCEL
   });
   return false;
 }

 showLedger =  false;
 ledgerData ={
  "headerContent":[
    {
      "key":"state",
      "label":"STATE"
    },
    {
      "key":"location",
      "label":"LOCATION"
    },
    {
      "key":"status",
      "label":"STATUS"
    },
    {
      "key":"product_handler",
      "label":"PRODUCT HANDLER"
    },
    {
      "key":"date",
      "label":"DATE"
    }
  ],
  "bodyContent":[
     {
       "state":"NY",
       "location":"New York",
       "status":"Recieved",
       "product_handler":"ABC Warehousing",
       "date":"10.10.16"
     },
     {
       "state":"PA",
       "location":"New York",
       "status":"Recieved",
       "product_handler":"ABC Warehousing",
       "date":"10.10.16"
     },
     {
       "state":"US",
       "location":"New York",
       "status":"Verification Failed",
       "product_handler":"ABC Warehousing",
       "date":"10.10.16"
     },
     {
      "state":"US",
      "location":"New York",
      "status":"Recieved",
      "product_handler":"ABC Warehousing",
      "date":"10.10.16"
    },
    {
      "state":"US",
      "location":"New York",
      "status":"Shipped",
      "product_handler":"ABC Warehousing",
      "date":"10.10.16"
    }
  ]
 }
 toggleLedger(){
  this.showLedger =  !this.showLedger;
 }

 product_properties = [
  {
    "key":"Product Name",
    "value":"Product 1"
  },
  {
    "key":"GTIN/Serial_Number",
    "value":"003003183912922938339"
  },
  {
    "key":"Lot_Number",
    "value":"A10011"
  },
  {
    "key":"Expiration Date",
    "value":"2105311"
  },
        {
    "key":"Expiration Date",
    "value":"2105311"
  }
 ]

 verifyAnotherProduct(){
  this.verified = false;
 }

 headers = undefined;
 verify(){
  // this.verified=true;
  console.log("acknowledgeData is ",this.acknowledge_data);
  let inputRequest = this.acknowledge_data;
  this._commonHttpService.commonHttpPostRequest(AppSettings.URL.ACKNOWLEDGE_CONSIGNMENT ,inputRequest, this.headers).subscribe(result =>{
    this.verified=true;
  });
 }

 continueAction(){
   this.showModal = false;
   this._router.navigate(['landing/dashboard'])
 }

}
