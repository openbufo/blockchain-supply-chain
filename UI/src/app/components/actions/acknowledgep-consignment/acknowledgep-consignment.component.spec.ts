import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcknowledgepConsignmentComponent } from './acknowledgep-consignment.component';

describe('AcknowledgepConsignmentComponent', () => {
  let component: AcknowledgepConsignmentComponent;
  let fixture: ComponentFixture<AcknowledgepConsignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcknowledgepConsignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcknowledgepConsignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
