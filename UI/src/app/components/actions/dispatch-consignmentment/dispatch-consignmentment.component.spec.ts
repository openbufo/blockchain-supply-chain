import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispatchConsignmentmentComponent } from './dispatch-consignmentment.component';

describe('DispatchConsignmentmentComponent', () => {
  let component: DispatchConsignmentmentComponent;
  let fixture: ComponentFixture<DispatchConsignmentmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispatchConsignmentmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispatchConsignmentmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
