import { Component, OnInit } from '@angular/core';
import { StorageService} from '../../../services/storage.service';
import { DashboardService} from '../../../services/dashboard.service';

@Component({
  selector: 'app-view-verifications',
  templateUrl: './view-verifications.component.html',
  styleUrls: ['./view-verifications.component.css']
})
export class ViewVerificationsComponent implements OnInit {

  constructor( private _storage : StorageService,
  private _dashboardService:DashboardService) { }
  verification_data;

  userType;
  showModal=false;
  notifCount=0;
  ngOnInit() {
    let userDetails = this._storage.api.local.get("currentUser");
    this._dashboardService.getViewVerificationsData(userDetails).subscribe(result => {
      this.verification_data = result.content;
    });
    this.userType = userDetails.principal_role;
  }

  showVerifyProductModal(){
    this.showModal= true;
  }
}
