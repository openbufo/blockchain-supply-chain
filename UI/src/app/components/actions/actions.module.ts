import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewVerificationsComponent} from './view-verifications/view-verifications.component'
import { ActionsComponent} from './actions.component';
import { ActionsRoutingModule} from './actions-routing/actions-routing.module'
import { KpmgGridModule} from '../shared/kpmg-grid/kpmg-grid.module';
import { VerifyProductComponent } from './verify-product/verify-product.component';
import { ShipProductComponent } from './ship-product/ship-product.component';
import { PublishShipmentComponent } from './publish-shipment/publish-shipment.component'
import { FormsModule } from '@angular/forms';
import { DashboardService} from '../../services/dashboard.service';
import { AcknowledgepConsignmentComponent } from './acknowledgep-consignment/acknowledgep-consignment.component';
import { DispatchConsignmentmentComponent } from './dispatch-consignmentment/dispatch-consignmentment.component';
import { ShipConsignmentComponent } from './ship-consignment/ship-consignment.component';
import { ValidateConsignmentComponent } from './validate-consignment/validate-consignment.component';
import { ReceiveConsignmentComponent } from './receive-consignment/receive-consignment.component';
import { CreateConsignmentComponent } from './create-consignment/create-consignment.component';
import { CommonHttpService } from '../../services/comman-http-request.service'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ActionsRoutingModule,
    KpmgGridModule
  ],
  declarations: [
    ViewVerificationsComponent,
    ActionsComponent,
    VerifyProductComponent,
    ShipProductComponent,
    PublishShipmentComponent,
    AcknowledgepConsignmentComponent,
    DispatchConsignmentmentComponent,
    ShipConsignmentComponent,
    ValidateConsignmentComponent,
    ReceiveConsignmentComponent,
    CreateConsignmentComponent
  ],
  providers:[
    DashboardService,
    CommonHttpService
  ]
})
export class ActionsModule { }
