import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipConsignmentComponent } from './ship-consignment.component';

describe('ShipConsignmentComponent', () => {
  let component: ShipConsignmentComponent;
  let fixture: ComponentFixture<ShipConsignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipConsignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipConsignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
