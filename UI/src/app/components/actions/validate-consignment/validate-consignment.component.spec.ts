import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateConsignmentComponent } from './validate-consignment.component';

describe('ValidateConsignmentComponent', () => {
  let component: ValidateConsignmentComponent;
  let fixture: ComponentFixture<ValidateConsignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidateConsignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateConsignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
