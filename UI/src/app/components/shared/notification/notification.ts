import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';
import { NotificationDataComponent } from "./notification-data/notification-data.component";

@Component({
  selector: 'notifications',
  templateUrl: './notification.html',
  styleUrls: ['./notification.css']
})
export class Notifications implements OnInit {

  @Input() data;
  @Output() showDetails:EventEmitter<any>;
  constructor() {
    this.showDetails =  new EventEmitter<any>();
   }

  ngOnInit() {

  }

  showDetail(eventObj){
    this.showDetails.emit({notification_id:eventObj.record_id});
  }

  /**for demo purpose putting jquery later will change*/
  highlightRecord(id){
    try { 
      let rec_id= 'record_'+id;
      $(".record").removeClass("highlight-record");
      $("#"+rec_id).addClass("highlight-record");
      this.showDetails.emit({notification_id:id}); 
    } catch (error) {
      console.log(error);
    }
  }
}
