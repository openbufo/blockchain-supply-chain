import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Notifications } from './notification';

describe('Notifications', () => {
  let component: Notifications;
  let fixture: ComponentFixture<Notifications>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Notifications ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Notifications);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
