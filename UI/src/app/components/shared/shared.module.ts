import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavHeaderBarComponent } from './nav-header-bar/nav-header-bar.component';
import { Notifications } from "./notification/notification";
import { NotificationDataComponent } from "./notification/notification-data/notification-data.component";
import { DataTablesModule } from 'angular-datatables';
// import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import { DatePickerModule } from 'ng2-datepicker';
import { InlineEditorModule } from 'ng2-inline-editor';     

import {SelectModule} from 'angular2-select';


@NgModule({ 
  imports: [
    CommonModule,
    DatePickerModule,  
    InlineEditorModule,
    DataTablesModule,
    SelectModule
 
  ],
  declarations: [
    NavHeaderBarComponent,Notifications,NotificationDataComponent, 
  ],
  exports: [
    NavHeaderBarComponent,Notifications,NotificationDataComponent, 
  
  ]
})
export class SharedModule { }
// platformBrowserDynamic().bootstrapModule(SharedModule);