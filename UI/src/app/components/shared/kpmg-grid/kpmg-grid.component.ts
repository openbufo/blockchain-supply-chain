import { Component, OnInit,Input,Output ,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-kpmg-grid',
  templateUrl: './kpmg-grid.component.html',
  styleUrls: ['./kpmg-grid.component.css']
})
export class KpmgGridComponent implements OnInit {

  @Input() data;
  @Input() expand=false;
  @Input() popup=false;
  @Input() showDetail = false;
  @Input() search=false;
  @Input() expandKey ='';
  @Input() searchByColumnsDropdownItems=[];
  @Output() showDetails:EventEmitter<any>;
  @Output() emitPopupStatus:EventEmitter<any>;
  searchText:string="";
  public showModal: boolean = false;
  constructor() { 
    this.showDetails =  new EventEmitter<any>();
    this.emitPopupStatus =  new EventEmitter<any>();
  }

  ngOnInit() {
    let activeObject = this;
    this.data.headerContent.forEach(function(item,index){
      index==0?activeObject.selectedColumn = item.key:'';
      activeObject.searchByColumnsDropdownItems.push(item.key);
    });
    
    this.data.bodyContent.forEach(function(item,index){
      item['expand'] = false;
    });
  }

  selectedColumn;
  selectedSearchOption(columnName){
    this.selectedColumn = columnName.target.value;
  }

  curretnRowClicked=0;
  toggleRowExpansion(rowId){
    this.curretnRowClicked = rowId;
    this.data.bodyContent[rowId].expand = !this.data.bodyContent[rowId].expand;
  }
  openPopupDetails(row, rowId){
    this.emitPopupStatus.emit(row);
  }

  checkExpandAvailability(){
    if(this.data.bodyContent.length>0 && this.data.bodyContent[this.curretnRowClicked].expand){
      return true;
    }
  }

  getTextColor(text){
    try {     
      if(text == 'Shipped'){
        return 'shipped';
      }else if(text == 'Verification Failed'){
        return 'failed'
      }else if(text == 'Recieved'){
        return 'recieved'
      }else if(!text){
        return 'rem-border';
      } 
    } catch (error) {
      console.log(error);
    }
  }

  expandDetails = false;
  showDetailedStatus(key){
    this.expandDetails = true;
  }

  checkValue(status){
    if(status){
      return ''
    }else{
      return 'rem-border';
    }
  }

  recordDetails(id){
    this.showDetails.emit({'record_id':id});
  }
}
