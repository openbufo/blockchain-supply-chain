import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KpmgGridComponent } from './kpmg-grid.component';
import { SearchFilterPipe } from './kpmg-grid-search.pipe';
import { FormsModule,ReactiveFormsModule} from '@angular/forms'; 

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [KpmgGridComponent,SearchFilterPipe],
  exports:[KpmgGridComponent]
})
export class KpmgGridModule { }
