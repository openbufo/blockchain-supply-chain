import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpmgGridComponent } from './kpmg-grid.component';

describe('KpmgGridComponent', () => {
  let component: KpmgGridComponent;
  let fixture: ComponentFixture<KpmgGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KpmgGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpmgGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
