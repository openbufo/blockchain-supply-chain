//searchFilter
import { Pipe, PipeTransform,AfterViewInit } from '@angular/core';
import { Component, Inject, forwardRef } from '@angular/core';
import { KpmgGridComponent } from './kpmg-grid.component';
@Pipe({
  name: 'searchFilter',
  pure: true
})
export class SearchFilterPipe implements PipeTransform {
  status:boolean=false;
  constructor(@Inject(forwardRef(() => KpmgGridComponent)) public dataTable: KpmgGridComponent) {}
  
  transform(substituteItems: any, searchItem?: any): any {
    let columnName = this.dataTable.selectedColumn;
    try {
        if (searchItem === undefined || searchItem=== null) {
            return substituteItems;
          } else {
            let arr = this.getData(substituteItems,columnName,searchItem);
            if(arr.length==0){
              $("tbody").css('height','0px');
              
            }else{
              $("tbody").css('height','200px');
            }
            return arr;
          }  
    } catch (error) {
        
    }
  }

  getData(substituteItems,columnName,searchItem){
    return substituteItems.filter(function(sub){
        if(typeof(sub[columnName]) == 'object'){
         let val = /^[+-]?((\.\d+)|(\d+(\.\d+)?))$/.test(sub[columnName]['CurrentValue']);
         if(val ==true){
            if(parseFloat(sub[columnName]['CurrentValue']).toFixed(2).toString().toLowerCase().includes(searchItem.toString().toLowerCase())){
              return sub[columnName]['CurrentValue'].toString().toLowerCase().includes(searchItem.toString().toLowerCase());
            }
         }else{
          if(sub[columnName]['CurrentValue'].toString().toLowerCase().includes(searchItem.toString().toLowerCase())){
            return sub[columnName]['CurrentValue'].toString().toLowerCase().includes(searchItem.toString().toLowerCase());
          }
         }
        }else{
          let val = /^[+-]?((\.\d+)|(\d+(\.\d+)?))$/.test(sub[columnName]);
          if(val ==true){
             if(parseFloat(sub[columnName]).toFixed(2).toString().toLowerCase().includes(searchItem.toString().toLowerCase())){
               return sub[columnName].toString().toLowerCase().includes(searchItem.toString().toLowerCase());
             }
          }else{
            if(sub[columnName].toString().toLowerCase().includes(searchItem.toString().toLowerCase())){
              return sub[columnName].toString().toLowerCase().includes(searchItem.toString().toLowerCase());
            }
          }
        }
      });
    }
}