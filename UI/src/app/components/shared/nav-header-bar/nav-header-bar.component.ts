import { Component, OnInit } from '@angular/core';
import { StorageService } from '../../../services/storage.service';
import { Observable } from 'rxjs/Observable';
import {Paho} from '../../../../../node_modules/ng2-mqtt/mqttws31';
import { Http, Response } from '@angular/http';
import { Router, ActivatedRoute, Params  } from '@angular/router';
import { AuthHttpService } from '../../../services/auth-http.service';
import 'rxjs/Rx';
import { AppSettings } from '../../../configuration/AppSettings';
import { AllsetupService } from '../../../services/allsetup.service';

@Component({
  selector: 'app-nav-header-bar',
  templateUrl: './nav-header-bar.component.html',
  styleUrls: ['./nav-header-bar.component.css'],
  providers: [AuthHttpService, AllsetupService, StorageService]
})
export class NavHeaderBarComponent implements OnInit {
  deviceData: any= [];
  userName:String = "Admin";
  userType:String ;
  activeTab:String = 'dashboard';
  myPlaceholderText: string = 'Select device IP...';
  constructor(private _service : AllsetupService, public router: Router, public _storage: StorageService) {
  }
  ngOnInit() {
    let userDetails = this._storage.api.local.get("currentUser");
    console.log("user details are ",userDetails);
    
    this.userName = userDetails.principal_user;
    this.userType = userDetails.principal_role;
  }
  onLoggedout() {
    this._storage.api.local.clear();
    this._storage.api.session.clear()
    this.router.navigate(['login']);
  }
  
  showmenu(tab){
    if(this.userType == "miner"){
      if(tab === 'Acknowledge Consignment' || tab === 'Dispatch Consignment'){
        return true;
      }else{
        return false;
      }
    }else if(this.userType == "transporter"){
      if(tab === 'Acknowledge Consignment' || tab === 'Ship Consignment' ){
        return true;
      }else{
        return false;
      }
    }else if(this.userType == "customer"){
      if(tab === 'Validate consignment' || tab === 'Receive Consignment' || tab === 'Create Consignment'){
        return true;
      }else{
        return false;
      }
    }else{
      return false;
    }
  }
  showActions(){
    if(this.userType == "miner" || this.userType == "transporter") {
      return ""
    }else{
      return "disabled";
    }
  }

  showNotifications(){
   return ""
  }

  navigateNotifiactions(){
    this.activeTab = 'notifications';
    this.router.navigate(['landing/notifications']);
  }
  
  navigateActions(routeUrl){
    console.log("route url is ",routeUrl);
    if(this.userType == "miner" || this.userType == "transporter" || this.userType == "customer") {
      this.activeTab = 'actions';
      this.router.navigate(['landing/actions/'+routeUrl]);
    }
  }

  navigateDashboard(){
    this.activeTab = 'dashboard';
    this.router.navigate(['landing/dashboard']);
  }

  isActive(tab){
    if(tab == this.activeTab){
      return 'active'
    }else{
      return ''
    }
  }
}
