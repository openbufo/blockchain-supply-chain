import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotificationsComponent} from '../notifications.component'
import { AuthGuardService } from '../../../services/auth-guard.service';

const routes: Routes = [
  { 
    path: '', component: NotificationsComponent,
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class NotificationRoutingModule { }

