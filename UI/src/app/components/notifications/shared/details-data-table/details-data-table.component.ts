import { Component, OnInit,Input,Output } from '@angular/core';

@Component({
  selector: 'app-details-data-table',
  templateUrl: './details-data-table.component.html',
  styleUrls: ['./details-data-table.component.css']
})
export class DetailsDataTableComponent implements OnInit {

  @Input() data;
  @Input() action=false;
  @Input() expandKey ='';


  constructor() { }

  ngOnInit() {
    this.data.bodyContent.forEach(function(item,index){
      item['expand'] = false;
    });
  }

  curretnRowClicked=0;
  toggleRowExpansion(rowId){
    this.curretnRowClicked = rowId;
    this.data.bodyContent[rowId].expand = !this.data.bodyContent[rowId].expand;
  }

  checkExpandAvailability(){
    if(this.data.bodyContent.length>0 && this.data.bodyContent[this.curretnRowClicked].expand){
      return true;
    }
  }

  getTextColor(text){
    try {     
      if(text == 'Shipped'){
        return 'shipped';
      }else if(text == 'Verification Failed'){
        return 'failed'
      }else if(text == 'Recieved'){
        return 'recieved'
      } 
    } catch (error) {
      console.log(error);
    }
  }

  expandDetails = false;
  showDetailedStatus(key){
    this.expandDetails = true;
  }
}
