import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationMetadataComponent } from './notification-metadata.component';

describe('NotificationMetadataComponent', () => {
  let component: NotificationMetadataComponent;
  let fixture: ComponentFixture<NotificationMetadataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationMetadataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationMetadataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
