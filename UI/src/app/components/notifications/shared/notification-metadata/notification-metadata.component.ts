import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-notification-metadata',
  templateUrl: './notification-metadata.component.html',
  styleUrls: ['./notification-metadata.component.css']
})
export class NotificationMetadataComponent implements OnInit {

  @Input() metadata;
  @Output() recordDetailsEvent:EventEmitter<any>;
  constructor() {
    this.recordDetailsEvent = new EventEmitter<any>();
  }

  ngOnInit() {
    console.log("inside metadat event ",this.metadata);
  }
 
  getNotificationStatus(status){
    try {   
      if(status == 'code_red'){
        return 'fa-exclamation-triangle red';
      }else if(status == 'code_yellow'){
        return 'fa-exclamation-triangle yellow'
      }else if(status == 'code_green'){
        return 'fa fa-check-circle green'
      } 
    } catch (error) {
      console.log(error);
    }
  }

  recordDetails(id){
    this.recordDetailsEvent.emit({record_id:id})
  }
}
