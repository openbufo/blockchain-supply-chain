import { Component, OnInit ,Input,Output} from '@angular/core';

@Component({
  selector: 'app-notification-details',
  templateUrl: './notification-details.component.html',
  styleUrls: ['./notification-details.component.css']
})
export class NotificationDetailsComponent implements OnInit {

  @Input() recordDetailData;
  action=false;
  newData;
  constructor() { }

  ngOnInit() {
    console.log("notifications details is ",this.recordDetailData);
  }
}
