import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationRoutingModule} from './notification-route/notification.route';
import { NotificationsComponent} from './notifications.component';
import { NotificationDetailsComponent} from './notification-details/notification-details.component'
import { DetailsDataTableComponent} from './shared/details-data-table/details-data-table.component'
import { NotificationMetadataComponent} from './shared/notification-metadata/notification-metadata.component' 
import { KpmgGridModule} from '../shared/kpmg-grid/kpmg-grid.module'
import { SharedModule} from '../shared/shared.module'
import { DashboardService} from '../../services/dashboard.service'

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    KpmgGridModule,
    NotificationRoutingModule
  ],
  declarations: [
     DetailsDataTableComponent,
     NotificationsComponent,
     NotificationDetailsComponent,
     NotificationMetadataComponent,
  ],
  providers:[DashboardService]
})
export class NotificationsModule { }
