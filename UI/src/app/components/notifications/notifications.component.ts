import { Component, OnInit } from '@angular/core';
import { DashboardService} from '../../services/dashboard.service';
import { StorageService} from '../../services/storage.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {

  constructor( private _dashboardService:DashboardService,
  private _storage:StorageService) { }

  readdata; 
  unreaddata;
  userDetails;
  sampleDetailData;
  ngOnInit() {
    this.recordDetailData = {
      "metadata":{},
      "configurations":{},
      "tableData":{
         "headerContent":[],
         "bodyContent":[]
       }
    };

    this.userDetails = this._storage.api.local.get("currentUser");
    this._dashboardService.getNotificationsList(this.userDetails).subscribe(result =>{
      this.readdata = result.content.readdata;
      this.unreaddata = result.content.unreaddata;
    });
  } 

  recordDetailData;
  detailNotAvailable = false;
  showDetailData = true;
  showDetails(id){
    let activeObject = this;
    try {
      this.showDetailData = false;
      let inputRequest = {
        "userDetails":this.userDetails,
        "notification_id":id['notification_id']
      }
      this._dashboardService.getNotificationsDetails(inputRequest).subscribe(
        result =>{
          this.recordDetailData = result.content["all_notifications"][0];
          console.log("record details data is ",this.recordDetailData);

          if(result.content=={}){
            this.detailNotAvailable = true;
          }
          setTimeout(function(){
            activeObject.showDetailData = true; 
          },200);
        }
      )
     
    } catch (error) {
      console.log(error);
    }
  }
}
