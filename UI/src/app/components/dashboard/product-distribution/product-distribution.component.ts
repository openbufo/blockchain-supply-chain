import { Component, OnInit ,Input} from '@angular/core';
import { StorageService} from '../../../services/storage.service';
import {DashboardService} from '../../../services/dashboard.service';
import { AppSettings} from '../../../configuration/AppSettings';

@Component({
  selector: 'product-distribution',
  templateUrl: './product-distribution.component.html',
  styleUrls: ['./product-distribution.component.css']
})
export class ProductDistributionComponent implements OnInit {

  constructor( private _storage:StorageService, private _dashboardService:DashboardService ) { }
  @Input() prodData;
  production_distribution_data = [];
  production_dropdowns

  userType;
  ngOnInit() {
    let userDetails = this._storage.api.local.get("currentUser");
    let URL = AppSettings.URL.PRODUCT_DISTRIBUTION
    this._dashboardService.getDashboardData(URL,userDetails).subscribe(result => {
      this.production_dropdowns = result.content.production_distribution;
      // this.production_distribution_data = result.content.production_distribution.chartData.stack_chart;
      // this.userType = userDetails.principal_role;
      // this.generateProductionStackedChart();
    });
    this.production_distribution_data =this.prodData;
    this.generateProductionStackedChart();
  }

  groupBy(inputArray, inputField){
    try {
      const groupedObj = inputArray.reduce((prev, cur)=> {
        if(!prev[cur[inputField]['Value']]) {
          prev[cur[inputField]['Value']] = [cur];
        } else {
          prev[cur[inputField]['Value']].push(cur);
        }
        return prev;
      }, {});
      return Object.keys(groupedObj).map(key => ({ key, value: groupedObj[key].length }));  
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  milestoneStackedChart;
  generateProductionStackedChart(){
    this.milestoneStackedChart=undefined;
    try {
      let series = [];
      let tempObj = {};
      for(let i=0;i<this.production_distribution_data.length;i++){
        tempObj ={};
        tempObj = {
          'name': this.production_distribution_data[i]['distributor']['key'],
          'data': this.production_distribution_data[i]['distributor']['value'],
          'color':this.production_distribution_data[i]['distributor']['color'],
        }
        series.push(tempObj)
      }

      console.log("seroies is",series);
      let chartXaxis="";
      this.milestoneStackedChart =  {
        'xCategory': [chartXaxis],
        'seriesData': series,
        'height': "110px"
      }

      let activeObject = this;
      setTimeout(function(){
        $('.highcharts-grid-line').remove();
        $('.highcharts-tick').remove();
        $('.highcharts-axis-line').remove();
      },200)
    } catch (error) {
      console.log(error);
      this.milestoneStackedChart = undefined;
    }
    
  }
}
