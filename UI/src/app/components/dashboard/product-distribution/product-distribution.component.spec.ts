import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDistributionComponent } from './product-distribution.component';

describe('ProductDistributionComponent', () => {
  let component: ProductDistributionComponent;
  let fixture: ComponentFixture<ProductDistributionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductDistributionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDistributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
