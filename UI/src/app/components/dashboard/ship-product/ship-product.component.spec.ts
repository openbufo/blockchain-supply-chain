import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipProductComponent } from './ship-product.component';

describe('ShipProductComponent', () => {
  let component: ShipProductComponent;
  let fixture: ComponentFixture<ShipProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShipProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
