import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService} from '../../../services/dashboard.service';
import { StorageService} from '../../.../../../services/storage.service';

@Component({
  selector: 'app-audit',
  templateUrl: './audit.component.html',
  styleUrls: ['./audit.component.css']
})
export class AuditComponent implements OnInit {
  public showModal: boolean = false;
  public auditDetails: any = {};

  constructor( private _router:Router,private _dashboardService:DashboardService,
    private _storage:StorageService) { }
  
    tableData;
  
    ngOnInit() {
      try {
        let userDetails = this._storage.api.local.get("currentUser");
        this._dashboardService.getAuditData(userDetails).subscribe(result => {
          if(result){
            console.log("result is ",result)
             this.tableData = result.content;
           }
         });
      } catch (error) {
        console.log(error);
      }
    }

    getPopupStatus(event){
      this.showModal = true;
      this.auditDetails = event;
    }

    saveAudit(){
      this.showModal = false;
    }

    closePopup(rowId){
      this.showModal = false;
    }
  
    navigateToDashboard(){
      this._router.navigate(['/landing/dashboard']);
    }
}
