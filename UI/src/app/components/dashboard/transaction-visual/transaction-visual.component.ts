import { Component, OnInit, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CommonHttpService } from  '../../../services/comman-http-request.service';
import { AppSettings } from '../../../configuration/AppSettings';

@Component({
  selector: 'app-transaction-visual',
  templateUrl: './transaction-visual.component.html',
  styleUrls: ['./transaction-visual.component.css']
})
export class TransactionVisualComponent implements OnInit {

  constructor(public _router: Router,private _commonHttpService:CommonHttpService) { }

  @Input() mapData;
  @Input() metaData;
  @Input() destination;
  @Input() currentWeight;
  @Input() requiredWeight;
  public _headers = undefined;

  ngOnInit() {
   console.log("map data is ",this.mapData);
   console.log("metadata is ",this.metaData);
  }

  navigateToTransactions(){
    try {
      this._router.navigate(['landing/product']);
    } catch (error) {
      console.log(error);
    }
  }
}
