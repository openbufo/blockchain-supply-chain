import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionVisualComponent } from './transaction-visual.component';

describe('TransactionVisualComponent', () => {
  let component: TransactionVisualComponent;
  let fixture: ComponentFixture<TransactionVisualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransactionVisualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionVisualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
