import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthHttpService } from '../../services/auth-http.service';
import { StorageService } from '../../services/storage.service';
import { AppSettings } from '../../configuration/AppSettings';
import { HttpModule,Http } from '@angular/http';
import { Notifications } from "../shared/notification/notification";
import { NotificationDataComponent } from "../shared/notification/notification-data/notification-data.component";
import { ReactiveFormsModule,FormGroup,FormControl,Validators,NG_VALIDATORS } from '@angular/forms';
import { AllsetupService } from '../../services/allsetup.service';
import { DashboardService} from '../../services/dashboard.service';
import { CommonHttpService } from  '../../services/comman-http-request.service';
import { GeoDirectionService } from '../../services/geo-direction.service'
import { Subject } from 'rxjs/Subject';

declare var require : any;
var Highcharts = require('highcharts');
var hMl = require('highcharts-multicolor-series/js/multicolor_series')(Highcharts);
  
@Component({ 
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [ AuthHttpService, StorageService, AllsetupService]
})
export class DashboardComponent implements OnInit {
  constructor(private _http : Http, public _geoService:GeoDirectionService, public _router: Router, private _commonHttpService:CommonHttpService,
    private _dashboardService:DashboardService,public _storage: StorageService,private _service: AllsetupService) { }
  unreaddata;

  private _transactionChanged: Subject<any> = new Subject<any>();
  public reloadProd=true;
  public production_distribution_data;
  public userType="";
  public dashboardTitle="";
  public permissionObject = "";
  public dashboardBlocks: any = [];
  public mapData;

  ngOnInit() {
    try {
      let userDetails = this._storage.api.local.get("currentUser");
      let URL = AppSettings.URL.ALERTS_NOTIFICATIONS
      this._dashboardService.getDashboardData(URL,userDetails).subscribe(result => {
        this.unreaddata = result.content.alerts_notifications;
        // if(result.content.production_distribution){
        //   this.production_distribution_data = result.content.production_distribution.chartData.stack_chart;
        // }
      
      });

      let URL2 = AppSettings.URL.PRODUCT_DISTRIBUTION
      this._dashboardService.getDashboardData(URL2,userDetails).subscribe(result => {
        // this.production_dropdowns = result.content.production_distribution;
        this.production_distribution_data = result.content.production_distribution.chartData.stack_chart;
        console.log("datsa" ,this.production_distribution_data);
        this.reloadProd =true;
        // this.userType = userDetails.principal_role;
        // this.generateProductionStackedChart();
      });

      this.userType = userDetails.principal_role;
      this.userType == 'miner'?this.dashboardTitle = 'Miner':'';
      this.userType == 'transporter'?this.dashboardTitle = 'Transporter':'';
      this.userType == 'customer'?this.dashboardTitle = 'Customer':''; 
      this.userType == 'auditor'?this.dashboardTitle = 'Auditor':''; 

      this.getCurrentTransactions();

      // setInterval(()=>{
      //   this.getCurrentTransactions();
      // },20000);

      // get weight map data
    } catch (error) {
      console.log(error);
    }
    this.fetchDashboardTiles();
  } 

  public changeGDTD =[2,1,3];
  public lineChartData = [{
    data: [-10, -5, 0, 5, 10, 15, 10, 10, 5, 0, -5],
    zones: [{
        value: 0,
        color: '#f7a35c'
    }, {
        value: 10,
        color: '#7cb5ec'
    }, {
        color: '#90ed7d'
    }]
  }];

  public transaction_details ={
    "metaData":{},
    "location_list":[],
    "weight_list":[]
  }

  public current_transactions;
  public _headers = undefined;

  checkIfData(key){
    try {
      if(key == 'metadata' && Object.keys(this.transaction_details.metaData).length>0){
        return true;
      }else if(key == 'location_list' &&  this.transaction_details.location_list.length>0){
        return true;
      }else if(key == 'weight_list' && this.transaction_details.weight_list.length>0){
        return true;
      }
    } catch (error) {
      console.log(error);
    }
  }
  /** get current transactions */
  getCurrentTransactions(){
    try {
      console.log("inside transactions");
      this.current_transactions = undefined;
      let inputRequest = {};
      this._commonHttpService.commonHttpGetRequest(AppSettings.URL.GET_CURRENT_TRANSACTIONS,this._headers).subscribe(result=>{
        console.log("result is ",result);
        this.dataFormatter(result)
        // this.current_transactions = result;
        console.log("current transaction are ",this.current_transactions);
        this.getWeightMapData();
      }); 
    } catch (error) {
      console.log(error);
    }
  }

  dataFormatter(result){
    try {
      let data = {};
      data['transactions'] = [];
      result.forEach((item,index)=>{
        console.log("transaco item i s ",item);
        
        let obj = {};
        obj['number'] = item['consignmentId'];
        obj['train'] = item['content'];
        obj['status'] = item['currentStatus'];
        obj['currGrade'] = item['aaa'];
        obj['reqGrade'] = item['orderedQualityGrade'];
        obj['loadCell'] = item['ssss'];
        obj['destStn'] = item['destination']['name'];
        obj['reqWeight'] = item['orderedQuantity']['value'];
        
        if(item['weightList'] && item['weightList'][0]){
          obj['currweight'] = item['weightList'][0]['value'];
        }
        if(item['locationList'] && item['locationList'][0]){
          console.log("inside else if ",item);
          obj['currStn'] = item['locationList'][0]['name']; //TODO
        }
        if(item['weightList'] && item['weightList'][0]){
          obj['lastUpdated'] = item['weightList'][0]['fetchTime'];
        }
        data['transactions'].unshift(obj);
        
        /** let's begin */
        let detail = item['consignmentId'];
        data[detail] = {};
        data[detail]['metaData'] ={};
        //metdata
        data[detail]['metaData']['auditor'] = 'Auditor';
               
        data[detail]['metaData']['requiredGrade'] = item['orderedQualityGrade'];
        data[detail]['metaData']['requiredWeight'] = item['orderedQuantity']['value'];
        data[detail]['metaData']['miner'] = "Miner";
        data[detail]['metaData']['destination'] = {} ;

        data[detail]['metaData']['destination']['lat'] = parseFloat(item['destination']['latitude']);
        data[detail]['metaData']['destination']['long'] = parseFloat(item['destination']['longitude']);
        data[detail]['metaData']['destination']['station_name'] =item['destination']['name'];

       if(item['locationList'] && item['locationList'][0]){
          data[detail]['metaData']['currentStation'] = item['locationList'][0]['name']; //TODO
        }else if( item['qualityList'] && item['qualityList'][0]){
          data[detail]['metaData']['currentGrade'] =  item['qualityList'][0]['value'];
        }
        /** metaData ends here */
        console.log("current data after metadata formatting is ",data);

        /** location list and weight list  */
        data[detail]['location_list'] = [];
        data[detail]['weight_list'] = [];

        console.log("item weight list is ", item['weightList']);
        
        if(item['weightList']){
          item['weightList'].forEach((subItem,subInd)=>{
            let weightAndLocObj = {};
            console.log("sub ssdsdssddsdsdsdsdds ",subItem['weighLocation']);
            
            weightAndLocObj['current_weight'] = subItem['value'];
            weightAndLocObj['timestamp'] = subItem['fetchTime'];
            weightAndLocObj['station_info'] = {};

            if(subItem['weighLocation']){
              weightAndLocObj['station_info']['station_name'] = subItem['weighLocation']['name'] //TODO 
              weightAndLocObj['station_info']['station_code'] = subItem['weighLocation']['code'] //TODO 
            }
            data[detail]['weight_list'].unshift(weightAndLocObj);
          });

          //locations
          console.log("item to cechk fro locations ",item);
          
          item['locationList'].forEach((subItem,subInd)=>{
            let weightAndLocObj = {};
            console.log("sub item is ",subItem);
            
            if(subItem['latitude'] || subItem['longitude']){
              weightAndLocObj['lat'] = parseFloat(subItem['latitude']);
              weightAndLocObj['long'] = parseFloat(subItem['longitude']);
            }
            
            weightAndLocObj['current_weight'] = subItem['value'];
            weightAndLocObj['timestamp'] = subItem['fetchTime'];
            weightAndLocObj['station_info'] = {};
            weightAndLocObj['station_info']['station_name'] = subItem['name'] //TODO 
            weightAndLocObj['station_info']['station_code'] = subItem['code'] //TODO 
            
           
            data[detail]['location_list'].unshift(weightAndLocObj);
          });
        }
      });
      this.current_transactions = data;
      console.log("data is ",data);
    } catch (error) {
      console.log(error);
    }
  }
  /** get weight map data */
  getWeightMapData(){
    try {
     
      let inputRequest = {};
      console.log("first number is ",this.current_transactions['transactions'][0]['number']);
      let firstNumber = this.current_transactions['transactions'][0]['number'];
      this.getOrderDetails({id:firstNumber})
      console.log("transation details are ",this.transaction_details);
    } catch (error) {
      console.log(error);
    }
  }

  public lineChartAxisData = {'xCategory':[]};
  public chartComplete = false;
  formatWeightListDataForChart(weightListData,requireWeight){
    try { 
      console.log("wiehgt incoming is ",weightListData);
      // this.transaction_details.weight_list = [];
      let chartdata = [];
      weightListData.forEach((item,index)=>{
        let obj = {};
        obj['y'] = item.current_weight;
        obj['name'] = item.station_info.station_name;
        if(weightListData[index+1] && weightListData[index+1].current_weight < requireWeight){
          obj['segmentColor'] = "red"
        }else{
          obj['segmentColor'] = "green"
        }
        chartdata.push(obj);
        this.lineChartAxisData['xCategory'].push(item.timestamp);
      });
      // this.transaction_details.weight_list = chartdata;
      console.log("transaction lis now os weight ", this.transaction_details.weight_list);
      
      $('#line-container').remove();

      setTimeout(()=>{
        $('<div/>', {
          id: 'line-container',
          class: 'chart',
         
        }).appendTo('.chart-asdf .chartConatiner');
  
        setTimeout(()=>{
          var chart = new Highcharts.Chart({
            chart: {
              renderTo: 'line-container',
              type: 'coloredline',
              backgroundColor:"#2d3944",
              // width:this.lineChartInfo.width,
              height:300
            },
            title : { 
              text :"Load cell Live mapping ",
              style:{
                color:"#d3d6d8"
              }
            },
            xAxis: {
              categories: this.lineChartAxisData['xCategory'],
              labels: {
                style: {
                    color: '#fff'
                }
              }
            },

            yAxis:{
              labels: {
                style: {
                    color: '#fff'
                }
              }
            },
            plotOptions: {
              series: {
                  lineWidth: 2.5
              }
            },
            series: [{
              type: 'coloredline',
              data:chartdata
            }]
          });
        },200);
      },200);
      this.chartComplete = true;
    } catch (error) {
      console.log(error);
    }
  }

  /** Get order details */
  public currentId
  getOrderDetails(event){
    try {
      this.currentId = event.id
      this.transaction_details = undefined;
       this.chartComplete = true;
      setTimeout(()=>{
        this.transaction_details = JSON.parse(JSON.stringify(this.current_transactions[event.id]));
        setTimeout(()=>{this.formatWeightListDataForChart(this.transaction_details['weight_list'],this.current_transactions[event.id].metaData['requiredWeight'])
        },100) 
      },200);
    } catch (error) {
      console.log(error);
    }
  }

  /** fetch dashboard tiles */
  fetchDashboardTiles(){
    let miner = [
      { "label": "Pending Acknowledgements", "value": "8", "icon": "fa-clock-o" },
      { "label": "Dispatched Orders", "value": "4", "icon": "fa fa-train" },
      { "label": "In Transit Orders", "value": "6", "icon": "fa fa-cog" },
      { "label": "Completed Orders", "value": "10", "icon": "fas fa-tasks" }
    ];
    let transporter = [
      { "label": "Pending Acknowledgements", "value": "8", "icon": "fa-clock-o" },
      { "label": "Shipped Orders", "value": "4", "icon": "fa-clock-o" },
      { "label": "In Transit Orders", "value": "6", "icon": "fa fa-steering-wheel" },
      { "label": "Completed Orders", "value": "10", "icon": "fas fa-tasks" }
    ];
    let customer = [
      { "label": "In Progress Orders", "value": "8", "icon": "fa-clock-o" },
      { "label": "Pending Acknowledgements (with miner)", "value": "4", "icon": "fa-clock-o" },
      { "label": "Pending Acknowledgements (with transporter)", "value": "6", "icon": "fa-clock-o" },
      { "label": "Completed Orders", "value": "10", "icon": "fas fa-tasks" }
    ];
    let Auditor = [
      { "label": "Pending Inspections", "value": "8", "icon": "fa-clock-o" },
      { "label": "Completed Inspections", "value": "4", "icon": "fas fa-tasks" }
    ];

    this.userType == 'auditor' ?this.dashboardBlocks = Auditor:'';
    this.userType == 'miner' ?this.dashboardBlocks = miner:'';
    this.userType == 'transporter' ?this.dashboardBlocks = transporter:'';
    this.userType == 'customer' ?this.dashboardBlocks = customer:'';
    // let userDetails = this._storage.api.local.get("currentUser");
    // this._dashboardService.getDashboardData(AppSettings.URL.PRODUCT_DISTRIBUTION,userDetails).subscribe(result => {
    // this.dashboardBlocks = result.data;
    // });
  }

  changeProdDistData(data){
    try {
      let arr =[15];
      let activeObject = this;
      this.reloadProd=false;
      this.production_distribution_data.forEach(function(item,index){
        item['distributor']['value'] =  arr;
        arr[0] = arr[0]+Math.random()*index+2;
      }) 
      setTimeout(function(){
        activeObject.reloadProd=true;
      },200);  
    } catch (error) {
      console.log(error) 
    }
  } 
  
  navigateToRoute(e,routeUrl){
    this._router.navigate(['/landing/'+routeUrl]);
  } 
  navigateToTransactions(){
    try {
      this._router.navigate(['landing/product']);
    } catch (error) {
      console.log(error);
    }
  }
}
