import { Component, OnInit } from '@angular/core';
import { DashboardService} from '../../../services/dashboard.service';
import { AppSettings} from '../../../configuration/AppSettings';

@Component({
  selector: 'incoming-shipments',
  templateUrl: './incoming-shipment.component.html',
  styleUrls: ['./incoming-shipment.component.css']
})
export class IncomingShipmentComponent implements OnInit {

  incoming_shipments;
  
  constructor(private _dashboardService:DashboardService) { }
  userDetails;

  ngOnInit() {
    try {
      this.userDetails = JSON.parse(localStorage.getItem('currentUser'));
      let URL = AppSettings.URL.INCOMING_SHIPMENTS
      this._dashboardService.getDashboardData(URL,this.userDetails).subscribe(result => {
        this.incoming_shipments = result.content.incoming_shipments;
      });   
    } catch (error) {
      console.log(error);
    }
  }
}
