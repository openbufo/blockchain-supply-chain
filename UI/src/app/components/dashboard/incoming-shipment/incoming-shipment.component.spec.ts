import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncomingShipmentComponent } from './incoming-shipment.component';

describe('IncomingShipmentComponent', () => {
  let component: IncomingShipmentComponent;
  let fixture: ComponentFixture<IncomingShipmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncomingShipmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncomingShipmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
