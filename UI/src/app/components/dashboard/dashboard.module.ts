import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardRoutingModule} from './dashboard-routing/dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from "../shared/shared.module";
import { Notifications } from "../shared/notification/notification";
import { NotificationDataComponent } from "../shared/notification/notification-data/notification-data.component";
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import { KpmgGridModule} from '../shared/kpmg-grid/kpmg-grid.module';
import { HighchartModuleModule } from '../highchart-module/highchart-module.module';
import { DashboardService} from '../../services/dashboard.service';

import { CurrentProductLevelComponent } from './current-product-level/current-product-level.component';
import { ProductDistributionComponent } from './product-distribution/product-distribution.component';
import { DistributorPerformanceComponent } from './distributor-performance/distributor-performance.component';
import { ShipProductComponent } from './ship-product/ship-product.component';
import { VerifyProductComponent } from './verify-product/verify-product.component';
import { IncomingShipmentComponent } from './incoming-shipment/incoming-shipment.component';
import { AllVerificationsComponent } from './all-verifications/all-verifications.component';
import { OrderSummaryComponent } from './order-summary/order-summary.component';
import { TransactionVisualComponent } from './transaction-visual/transaction-visual.component';
import { AuditComponent } from './audit/audit.component';
import { CommonHttpService } from  '../../services/comman-http-request.service';
import { GeoDirectionService } from '../../services/geo-direction.service'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HighchartModuleModule,
    ReactiveFormsModule,
    DashboardRoutingModule,
    KpmgGridModule,
    SharedModule    
  ],
  declarations: [
     DashboardComponent,
     CurrentProductLevelComponent, 
     ProductDistributionComponent, 
     DistributorPerformanceComponent, 
     ShipProductComponent, 
     VerifyProductComponent, 
     IncomingShipmentComponent, AllVerificationsComponent, OrderSummaryComponent, TransactionVisualComponent, AuditComponent
  ],
  providers: [ Notifications,GeoDirectionService, CommonHttpService,NotificationDataComponent,DashboardService ]
})
export class DashboardModule { }
