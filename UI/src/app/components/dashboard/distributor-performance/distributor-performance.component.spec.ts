import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DistributorPerformanceComponent } from './distributor-performance.component';

describe('DistributorPerformanceComponent', () => {
  let component: DistributorPerformanceComponent;
  let fixture: ComponentFixture<DistributorPerformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistributorPerformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DistributorPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
