import { Component, OnInit } from '@angular/core';
import {DashboardService} from '../../../services/dashboard.service';
import { AppSettings} from '../../../configuration/AppSettings';

@Component({
  selector: 'distributor-performance',
  templateUrl: './distributor-performance.component.html',
  styleUrls: ['./distributor-performance.component.css']
})
export class DistributorPerformanceComponent implements OnInit {
  distributor_performance
 
  constructor( private _dashboardService:DashboardService) { }
  userDetails;
  ngOnInit() {
    try {
      this.userDetails = JSON.parse(localStorage.getItem('currentUser'));
      let URL = AppSettings.URL.DISTRIBUTION_PERFORMANCE
      this._dashboardService.getDashboardData(URL,this.userDetails).subscribe(result => {
        this.distributor_performance = result.content.distributor_performance;
      }); 
    } catch (error) {
      console.log(error);
    }
  }
}
