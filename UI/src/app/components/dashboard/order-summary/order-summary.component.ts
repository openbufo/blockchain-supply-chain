import { Component, OnInit,Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-order-summary',
  templateUrl: './order-summary.component.html',
  styleUrls: ['./order-summary.component.css']
})
export class OrderSummaryComponent implements OnInit {

  constructor() { }

  @Input() currentTransactions;
  @Output() getOrderDetailsEvent = new EventEmitter()
  public stationUpdates: any = [];
  public transaction_list = [];
  
  ngOnInit() {
    try {
      this.stationUpdates = this.currentTransactions; 
    } catch (error) {
      console.log(error);
    }
  }

  getStatus(status){
    if(status.toLowerCase()=='completed'){
      return 'C'
    } else if(status.toLowerCase()=='halt'){
      return 'H'
    } else if(status.toLowerCase()=='on going'){
      return 'OG'
    }
  } 
  getOrderDetails(consignmentId){
    try {
      this.getOrderDetailsEvent.emit({id:consignmentId})
    } catch (error) {
      console.log(error);
    }
  }
}
