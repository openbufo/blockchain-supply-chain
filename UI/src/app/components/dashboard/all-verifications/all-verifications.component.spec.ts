import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllVerificationsComponent } from './all-verifications.component';

describe('AllVerificationsComponent', () => {
  let component: AllVerificationsComponent;
  let fixture: ComponentFixture<AllVerificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllVerificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllVerificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
