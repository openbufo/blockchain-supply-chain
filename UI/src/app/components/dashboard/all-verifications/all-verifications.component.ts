import { Component, OnInit } from '@angular/core';
import { DashboardService} from '../../../services/dashboard.service';
import { AppSettings} from '../../../configuration/AppSettings'

@Component({
  selector: 'app-all-verifications',
  templateUrl: './all-verifications.component.html',
  styleUrls: ['./all-verifications.component.css']
})
export class AllVerificationsComponent implements OnInit {

  constructor( private _dashboardService:DashboardService) { }

  verification_data; 
  ngOnInit() {
    let userDetails = JSON.parse(localStorage.getItem("currentUser"));
    let URL = AppSettings.URL.ALL_VERIFICATIONS;
    this._dashboardService.getDashboardData(URL,userDetails).subscribe(result => {
      this.verification_data = result.content.verification_data;
    });
  }
}
