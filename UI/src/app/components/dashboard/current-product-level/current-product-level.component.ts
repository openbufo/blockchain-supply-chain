import { Component, OnInit,Output,Input ,EventEmitter} from '@angular/core';
// import {} from '../../../services/comman-http-request.service';
import { Http } from '@angular/http';
import {AppSettings} from '../../../configuration/AppSettings'
import { DashboardService} from '../../../services/dashboard.service'

@Component({
  selector: 'current-product-level',
  templateUrl: './current-product-level.component.html',
  styleUrls: ['./current-product-level.component.css']
})
export class CurrentProductLevelComponent implements OnInit {

  @Output() changeProdDistData:EventEmitter<any> = new EventEmitter();
  current_product_level;

  treemapData = [
    {   
        "id":"21",
        "name": "Merck",
        "value": 4000,
        "color": "#00867b"
    },
    {   "id":"22",
        "name": "Distributor",
        "value": 1300,
        "color": "#175e99"
    },
    {   "id":"23",
        "name": "Dispenser",
        "value": 1211,
        "color": "#2e9452"
    }
  ]

  constructor(private _http:Http,
  private _dashboardService:DashboardService) {

  }

  current_product_chart;
  userDetails;
  ngOnInit() {
    try {
      let URL = AppSettings.URL.CURRENT_PRODUCT_LEVEL;
      
      this.userDetails = JSON.parse(localStorage.getItem('currentUser'));
      this._dashboardService.getDashboardData(URL,this.userDetails).subscribe(result => {
        this.current_product_level = result.content.current_product_level;
        this.current_product_chart={
          "series":this.current_product_level.chartData.treemapData,
          "height":"265px"
        }
      }); 
    } catch (error) {
      console.log(error);
    }
  }

  changeDistributionData(data){
    this.changeProdDistData.emit(data);
  }
}
