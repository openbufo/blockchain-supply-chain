import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentProductLevelComponent } from './current-product-level.component';

describe('CurrentProductLevelComponent', () => {
  let component: CurrentProductLevelComponent;
  let fixture: ComponentFixture<CurrentProductLevelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentProductLevelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentProductLevelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
