import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from '../landing-page.component';

const routes: Routes = [
  { 
    path: '', component: LandingPageComponent,
    children:[
      { path: '', loadChildren: 'app/components/dashboard/dashboard.module#DashboardModule'},
      { path: 'dashboard', loadChildren: 'app/components/dashboard/dashboard.module#DashboardModule'},
      { path: 'notifications', loadChildren: 'app/components/notifications/notifications.module#NotificationsModule'},
      { path: 'product', loadChildren: 'app/components/product/product.module#ProductModule'},
      { path: 'actions',loadChildren: 'app/components/actions/actions.module#ActionsModule'}
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class LandingPageRoutingModule { }

