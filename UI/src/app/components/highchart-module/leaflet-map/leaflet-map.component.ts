import { GeoDirectionService } from './../../../services/geo-direction.service';
import { Component, OnInit,Input,Output } from '@angular/core';
import L from 'leaflet';

@Component({
  selector: 'app-leaflet-map',
  templateUrl: './leaflet-map.component.html',
  styleUrls: ['./leaflet-map.component.css'],
})
export class LeafletMapComponent implements OnInit {

  @Input() myMapData
  @Input() destination
  mymap: any;
  marker: any;
  popup: any;
  polyline: any;
  leafIcon: any;
  greenIcon: any;

  constructor(private geoService: GeoDirectionService) { }


  onMapClick(e) {
    console.log(e)
    this.popup = L.popup()
      .setLatLng(e.latlng)
      .setContent("You clicked the map at " + e.latlng.toString())
      .openOn(this.mymap);

    
  }

  ngOnInit() {
    this.mymap = L.map('mapid').setView([23.40601, 79.45809], 5);
    const polylineDirection = [];
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      subdomains: ['a', 'b', 'c']
    }).addTo(this.mymap);

    console.log("destination coordinates are ",this.destination);
    
    console.log("myMpadata is ",this.myMapData);
    

    this.leafIcon = L.Icon.extend({
      options: {
        iconSize: [29, 24],
        iconAnchor: [9, 21],
        popupAnchor: [0, -14]
      }
    });

    var greenIcon = new this.leafIcon({
      iconUrl: 'assets/images/trnSrcStnMap.png',
    })
    var onGoing = new this.leafIcon({
      iconUrl: 'assets/images/live.gif'
    })

    var redIcon = new this.leafIcon({
      iconUrl: 'assets/images/queriedStnLegend.png',
    })

    var reachpoint = new this.leafIcon({
      iconUrl: 'assets/images/mapNotPassedStnLegend.png',
    })

    // GET GEO JSON
    // this.geoService.getGeoDirection().subscribe(list => {
    //     console.log("list is ",list);
        let location_list  = this.myMapData;
        
        console.log("",location_list[0]);
        L.marker([location_list[0].lat, location_list[0].long], { icon: greenIcon }).addTo(this.mymap);
        polylineDirection.unshift([location_list[0].lat, location_list[0].long])
        
        var nextLocation = location_list.splice(1);

        var extractLastLocation = nextLocation.pop();
        nextLocation.forEach((point, index) => {
          L.marker([point.lat, point.long], { icon: reachpoint }).addTo(this.mymap);
          polylineDirection.push([point.lat, point.long])
        });

        L.marker([extractLastLocation.lat, extractLastLocation.long], { icon: onGoing }).addTo(this.mymap);
        // polylineDirection.push([location_list, location_list]);
        polylineDirection.push([extractLastLocation.lat, extractLastLocation.long])

        console.log("poly line direction is ",polylineDirection);
        
        //Destination
        // L.marker([this.destination.lat, this.destination.long], { icon: redIcon }).addTo(this.mymap);
        // polylineDirection.push([this.destination.lat, this.destination.long])

        var myRenderer = L.svg({ padding: 0.5 });
        console.log(polylineDirection);

        this.polyline = L.polyline(polylineDirection, { weight: 4 }, { renderer: myRenderer }).addTo(this.mymap);
        this.mymap.fitBounds(this.polyline.getBounds());
      // })
    this.mymap.on('click', this.onMapClick);
  }


  updateCurrentLocation() {

  }

  updateVisitedStation() {

  }

}
