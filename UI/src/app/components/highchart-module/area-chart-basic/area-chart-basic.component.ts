import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'area-chart-basic',
  templateUrl: './area-chart-basic.component.html',
  styleUrls: ['./area-chart-basic.component.css']
})
export class AreaChartBasicComponent implements OnInit {
  @Input() areaChartInfo:any;
  chartOptions:Object; 
  constructor() { }
  ngOnInit() {
    this.chartOptions = {
      chart:{
        margin:[2, 0, 2, 0],
        width:60,
        height:20,
        backgroundColor:'transparent',
        style:{
          overflow:'visible'
        },
        skipClone :true
      },
      exporting:{
        enabled:false
      },
      title:{
        text:''
      },
      credits:{enabled:false},
      xAxis:{
        labels: {
          enabled: false
        },
        title:  {
          text: null
        },
        startOnTick: false,
        endOnTick: false,
        tickPositions: []
      },
      yAxis:{
        endOnTick: false,
        startOnTick: false,
        labels: {
          enabled: false
        },
        title: {
          text: null
        },
        tickPositions: [0]
      },
      legend:{enabled:false},
      tooltip: {
        backgroundColor: null,
        borderWidth: 0,
        shadow: false,
        useHTML: true,
        hideDelay: 0,
        shared: true,
        padding: 0,
        xDateFormat :'%Y-%m-%d',
        positioner: function (w, h, point) {
          return {x: point.plotX - w / 2, y: point.plotY - h};
        },
        headerFormat: '<span style="font-size: 10px">Timestamp: {point.key:%Y-%m-%d}</span><br/>',
        pointFormat: 'value : <b>{point.y}.000</b>'
      },
      plotOptions:{   
        series: {
          cursor: 'pointer',
          animation: false,
          lineWidth: 1,
          shadow: false,
          states: {
            hover: {
              lineWidth: 1
            }
          },
          marker: {
            radius: 1,
            states: {
              hover: {
                radius: 2
              }
            }
          },
          fillOpacity:0.25
        }
      },
      series: [{
        type:'area',
        data: this.areaChartInfo.trend_data
      }]
           
    }
  }
}