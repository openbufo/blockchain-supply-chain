import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'bar-chart-basic',
  templateUrl: './bar-chart-basic.component.html',
  styleUrls: ['./bar-chart-basic.component.css']
})
export class BarChartBasicComponent implements OnInit {
  @Input() barChartInfo: any;
  @Output() xaxisdata:EventEmitter<any>;
  @Output() yaxisdata:EventEmitter<any>;
  chartOptions:Object;
  barname:any;
  constructor() {
    this.xaxisdata = new EventEmitter<any>();
    this.yaxisdata = new EventEmitter<any>();
   }
  ngOnInit() {
 
      this.chartOptions = {
        chart: {
          type: 'column',
          // width:200,
          height:250
        },
        xAxis: {
          categories: this.barChartInfo.stations,
        /*   title: {
              enabled: true,
              text: 'Custom with <b>simple</b> <i>markup</i>',
              style: {
                  fontWeight: 'normal'
              }
          } */
      },
        title: {
          text: this.barChartInfo.chartName
        },
        credits: {
            enabled: false
        },
        exporting: { enabled: false },
        subtitle: {},
        
      
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        series: {
          cursor: 'pointer',
          point: {
              events: {
                  click:
                   //this.barchartsendingvalue.bind(this),
                  function (event) { 
                    let obj = {"location_name":event.point.category, "value":event.point.series.name}
                      this.xaxisdata.emit(obj);
                     // console.log('xaxis Data : ', this.xaxisdata);
                      //this.yaxisdata.emit(event.point.series.name);
                      //this.barchartsendingvalue.emit(event.point.y);
                      //console.log(event.point.y);
                      //console.log(event);
                      
                     // alert('Category: ' + event.point.category + ', value: ' + event.point.y+ ', Series :' + event.point.series.name);
                      //console.log(event.point.series.name);
                  //}
                   }.bind(this)
              }
          }
      },
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      series: this.barChartInfo.chartSeries
    };
  }
}