import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'stacked-bar-chart-basic',
  templateUrl: './stacked-bar-chart.component.html',
  styleUrls: ['./stacked-bar-chart.component.css']
})
export class StackedBarChart implements OnInit {
  @Input() chartInfo: any;
  chartOptions:Object;
  @Output() chartClick:EventEmitter<any> = new EventEmitter();
  @Output() chart:EventEmitter<any>;
  constructor() { 
    this.chart = new EventEmitter<any>();
  }
  ngOnInit() { 
    try {
      let activeObject = this;
      this.chartOptions = {
        chart: {
            type: 'bar',
            height: (this.chartInfo['height']?this.chartInfo['height']:undefined),
            backgroundColor: '#2d3944',
        },
        title: {
            text: ''
        },
        credits:{
          enabled: false
        },
        exporting:{
          enabled: false
        },
        xAxis: {
          // enabled: false,
          labels: {
              enabled: false
          }
          // categories: this.chartInfo['xCategory']
        },
        yAxis: {
            // min: 0,
            // title: {
            //     text: ''
            // }
            title: {
              text: false
            },
            labels: {
              enabled: false
          }
        },
        legend: {
            enabled:false,
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                borderWidth: 1,
                borderColor: 'black',
                strokeWidth: 0,
                
                // cursor: 'pointer',
                // point: {
                //   events: {
                //       click: function () {
                //           activeObject.getChartEvent(this,'barClick');
                //       }
                //   }
                // },
                dataLabels: {
                  enabled: false,
                  color: 'white',
                  formatter: function () {
                    return  this.series.name +" : <b>"+ this.y + "</b>";
                 }
              },
              events:{
                legendItemClick: function () {
                  activeObject.getChartEvent(this,'legendClick');
                }
              }
            }
        },
        series: this.chartInfo['seriesData']
      }
    } catch (error) {
      console.log(error);
    }
  }

  stackChartInstance;
  saveChartInstance(chartInstance){
    this.stackChartInstance = chartInstance;
    this.chart.emit(chartInstance);
  }
  
  getChartEvent(eventData,clickRef){
    try {
      this.chartClick.emit({eventData:eventData,clickRef:clickRef,legendAllItems:this.stackChartInstance.legend.allItems});
    } catch (error) {
      console.log(error);
    }
  }
}
