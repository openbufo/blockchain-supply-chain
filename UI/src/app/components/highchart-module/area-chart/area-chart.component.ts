import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-area-chart',
  templateUrl: './area-chart.component.html',
  styleUrls: ['./area-chart.component.css']
})
export class AreaChartComponent implements OnInit {
  chartOptions: Object;
  @Input() chartData;
  constructor() { }

  ngOnInit() {
    this.chartOptions = {
        credits:{ enabled:false },
        exporting: {
          enabled: false
        },
				chart: {
			        type: 'area'
			    },
			    title: {
			        text: ''
			    },
			    xAxis: {
			        categories : this.chartData['trendChartDateX'],
			        labels: {
			            	rotation: -40,
			            },
			        minPadding: 0,
			        maxPadding: 0,
			        tickmarkPlacement: 'on',
			        title: {
			            	enabled: false
			        	},
			    },
			    yAxis: {
			    		min:0,
			        title: {
			            text: ''
			        },
			        labels: {
			            
			        }
			    },
			    tooltip: {
			    		shared: true,
			        	split: false,
			        	valueSuffix: ''
			    },
			    plotOptions: {
			        area: {
			            //stacking: 'normal',
			            lineColor: '#666666',
			            lineWidth: 1,
			            marker: {
			                lineWidth: 1,
			                lineColor: '#666666'
			            }
			        },
			        series: {
			        	fillOpacity: 1,
				        marker: {
				            enabled: false,
					        symbol: 'circle'
					    }
					}
			    },
			    series: this.chartData.trendChartData
			}
  }

}
