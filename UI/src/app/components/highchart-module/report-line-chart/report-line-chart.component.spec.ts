import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportLineChartComponent } from './report-line-chart.component';

describe('ReportLineChartComponent', () => {
  let component: ReportLineChartComponent;
  let fixture: ComponentFixture<ReportLineChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportLineChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
