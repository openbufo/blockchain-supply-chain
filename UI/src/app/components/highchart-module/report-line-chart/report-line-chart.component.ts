import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'report-line-chart',
  templateUrl: './report-line-chart.component.html',
  styleUrls: ['./report-line-chart.component.css']
})
export class ReportLineChartComponent implements OnInit {

  @Input() lineChartInfo: any;
  @Output() chartInstance:EventEmitter<any>;
  //@Output() chartInstance:EventEmitter<any>;
  public chartName:String;
  public chartSeriesData:Array<any> = [];
  chartOptions:Object;

  constructor() { 
    this.chartInstance = new EventEmitter<any>();
  }

  ngOnInit() {
    // console.log("chart series data");
    // console.log(this.chartSeriesData);
    //this.chartName = this.lineChartInfo.chartName;
    this.chartSeriesData = this.lineChartInfo;
    // console.log("chart series data");
    // console.log(this.chartSeriesData);
    this.chartOptions = {
      chart: {
          type: 'line',
          height:this.lineChartInfo.height,
      },
      title : { 
        text : ''
      },
      credits: {
          enabled: false
      },
      xAxis: {
        type: 'datetime',
      },
      exporting: { enabled: false },
      series: this.chartSeriesData 
    };
  }

}
