import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'pie-chart-basic',
  templateUrl: './pie-chart-basic.component.html',
  styleUrls: ['./pie-chart-basic.component.css']
})
export class PieChartBasicComponent implements OnInit {
  @Input() pieChartInfo: any;
  @Input() PiechartWidth;
  @Input() chartHeight;
  @Output() xaxisdata:EventEmitter<any>;
  @Output() yaxisdata:EventEmitter<any>;
  public LegendOptions:boolean;
  public chartName:string;
  public chartSeriesData:Array<any> = [];
  chartOptions:Object;
  constructor() { 
    this.xaxisdata = new EventEmitter<any>();
    this.yaxisdata = new EventEmitter<any>();
  }

  ngOnInit() {
    this.chartName = this.pieChartInfo.chartName;
    this.chartSeriesData = this.pieChartInfo.chartSeries;
    this.chartOptions = {
      chart: {     
        //backgroundColor: null,
        type: 'pie',
        margin: [0, 0, 0, 0],
        width: this.PiechartWidth,
        height:this.chartHeight,
        marginLeft: 60,
        //marginTop: 100,
        //height: 400,
        //width:400,
       
        style: {
          overflow: 'visible'
       },
        skipClone: true
      },
      exporting: {
        enabled: false
      },
      title:{
        text: this.chartName
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        pie: {
          // allowPointSelect: true,
          cursor: 'pointer',
          point: {
            events: {
                click:
                 //this.barchartsendingvalue.bind(this),
                function (event) { 
                  let obj = {"location_name":event.point.name}
                    this.xaxisdata.emit(obj);
                    //console.log('Barchart Values :',this);
                    //console.log('xaxis Data : ', this.xaxisdata);
                    //this.yaxisdata.emit(event.point.series.name);
                    //this.barchartsendingvalue.emit(event.point.y);
                    //console.log(event.point.y);
                      //console.log(event.point.name);
                     //alert(this.name);
                   // alert('Category: ' + event.point.name + ', value: ' + event.point.y+ ', Series :' + event.point.series.name);
                    //console.log(event.point.series.name);
                //}
                 }.bind(this)
            }
        },
          dataLabels: {
           /*  enabled: false,
            format: '{y} unit', */
            
            fontSize: '2px !important',
            textOutline: "1px",
            distance: -10
          },
          //showInLegend: this.LegendOptions, 
          showInLegend: true,         
         
        },
        
      },
       legend: {
           itemStyle: {
               fontSize:'15px',
               font: '10pt Trebuchet MS, Verdana, sans-serif',
           },
            align: 'left',
            layout: 'vertical',
            verticalAlign: 'top',
            fontSize:'4px',
            x: 0,
            y: 0
        },
      tooltip: {
        pointFormat: '{point.percentage:.1f}%'
      },
      series: [{
        data: this.chartSeriesData
      }]
    };
  }

}
