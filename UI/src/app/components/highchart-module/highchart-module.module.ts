import { LeafletMapComponent } from './leaflet-map/leaflet-map.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';

import { StackedBarChart} from './stacked-bar-chart/stacked-bar-chart.component'
import { BarChartBasicComponent } from './bar-chart-basic/bar-chart-basic.component';
import { LineChartBasicComponent } from './line-chart-basic/line-chart-basic.component';
import { PieChartBasicComponent } from './pie-chart-basic/pie-chart-basic.component';
import { AreaChartBasicComponent } from './area-chart-basic/area-chart-basic.component';
import { TreeMapBasicComponent } from './tree-map-basic/tree-map-basic.component';
import { CustomLineBarChartComponent } from './custom-line-bar-chart/custom-line-bar-chart.component';
import { AreaChartComponent } from './area-chart/area-chart.component';
import { ScatterPlotChartComponent } from './scatter-plot-chart/scatter-plot-chart.component';
import { ReportLineChartComponent } from './report-line-chart/report-line-chart.component';
import { GeoDirectionService} from '../../services/geo-direction.service'

declare var require : any;
export function highchartsFactory() {
  const hc = require('highcharts');
  const dd = require('highcharts/modules/exporting');
  const tm = require('highcharts/modules/treemap');
  dd(hc);
  tm(hc);

  return hc;
}

@NgModule({
  imports: [
    CommonModule,
    ChartModule
  ],
  providers: [
    GeoDirectionService,
    {
      provide: HighchartsStatic,
      useFactory: highchartsFactory
    }
  ],
  declarations: [
    BarChartBasicComponent,
    LineChartBasicComponent,
    PieChartBasicComponent,
    StackedBarChart,
    AreaChartBasicComponent,
    TreeMapBasicComponent,
    CustomLineBarChartComponent,
    AreaChartComponent,
    ScatterPlotChartComponent,
    ReportLineChartComponent,
    LeafletMapComponent
  ],
  exports: [
    BarChartBasicComponent,
    LineChartBasicComponent,
    PieChartBasicComponent,
    AreaChartBasicComponent,
    TreeMapBasicComponent,
    CustomLineBarChartComponent,
    StackedBarChart,
    AreaChartComponent,
    ScatterPlotChartComponent,
    ReportLineChartComponent,
    LeafletMapComponent
  ]
})
export class HighchartModuleModule { }