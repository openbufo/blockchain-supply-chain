import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomLineBarChartComponent } from './custom-line-bar-chart.component';

describe('CustomLineBarChartComponent', () => {
  let component: CustomLineBarChartComponent;
  let fixture: ComponentFixture<CustomLineBarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomLineBarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomLineBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
