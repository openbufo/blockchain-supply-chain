import { Component, OnInit, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-custom-line-bar-chart',
  templateUrl: './custom-line-bar-chart.component.html',
  styleUrls: ['./custom-line-bar-chart.component.css']
})
export class CustomLineBarChartComponent implements OnInit, OnChanges {
  @Input() chartSettings;
  @Input() chartData;
  seriesData:Array<any>;  
  chartOptions: Object;
  constructor() { }

  ngOnInit() {
    
    this.chartOptions = {
      title: {
        text: ''
      },
      credits: {
        enabled: false
      },
      xAxis: {
        categories: this.chartData['lineChartXLabel']
      },
      labels: {},
      series: this.chartSettings
      }
  }
  ngOnChanges() {
    this.ngOnInit();
  }
}
