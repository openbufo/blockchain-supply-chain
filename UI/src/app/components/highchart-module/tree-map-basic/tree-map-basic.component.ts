import { Component, OnInit, OnChanges, Input, Output,EventEmitter } from '@angular/core';

@Component({
    selector: 'app-tree-map-basic',
    templateUrl: './tree-map-basic.component.html',
    styleUrls: ['./tree-map-basic.component.css']
})
export class TreeMapBasicComponent implements OnInit,OnChanges {
    chartOptions:Object;
    @Input() treeMapInfo;
    @Output() coreDetails:EventEmitter<any> = new EventEmitter();
    constructor() { }
    ngOnInit() {
        console.log("treemap info is ",this.treeMapInfo);
        this.chartOptions = {
            chart:{
                height: (this.treeMapInfo['height']?this.treeMapInfo['height']:undefined),
                backgroundColor: '#2d3944',
            },
            credits: { enabled:false },
            exporting: { enabled: false },
            series: [{
                type: 'treemap',
                layoutAlgorithm: 'squarified',
                allowDrillToNode: true,
                dataLabels: { 
                    enabled: false 
                },
                levelIsConstant: false,
                levels: [{
                    level: 1,
                    dataLabels: { 
                        enabled: true, 
                    },
                    borderWidth: 0,
                    bordercolor: "#999966"
                }],
                data: this.treeMapInfo.series    
            }],
            legend:{
                enabled:false
            },
           plotOptions: {
                series: {
                    fillOpacity: 0.5,
                    cursor: 'pointer',
                    events: {
                    //     click: function (event) {
                    //         if(event.point.node.children.length ==0){
                    //             console.log("event is");
                    //             console.log(event);
                    //             console.log("children length is");
                    //             console.log(event.point.node.children.length);
                    //             var showModal=true;
                    //             this.modalEvent.emit(showModal);    
                    //         }
                    //     this.chart.showLoading('<img src="assets/images/giphy.gif">'); 
                    //     var that = this;
                    //     setTimeout(function(){that.chart.hideLoading();},1000)
                    // }
                    click: (event)=>this.getData(event)
                    
                    }
                }
            },
            title: {
                text: ''
            }
        }
         window.scrollTo(0, 10);
    }
    ngOnChanges() {
        this.ngOnInit();
    }
  
    getData(event){
        this.coreDetails.emit(event.point.name);
    }
}
