import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeMapBasicComponent } from './tree-map-basic.component';

describe('TreeMapBasicComponent', () => {
  let component: TreeMapBasicComponent;
  let fixture: ComponentFixture<TreeMapBasicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeMapBasicComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeMapBasicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
