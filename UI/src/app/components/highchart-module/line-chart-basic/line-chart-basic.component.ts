import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'line-chart-basic',
  templateUrl: './line-chart-basic.component.html',
  styleUrls: ['./line-chart-basic.component.css']
})
export class LineChartBasicComponent implements OnInit {
  @Input() lineChartInfo: any;
  @Input() lineChartAxisData;
  @Output() chartInstance:EventEmitter<any>;
  public chartName:String;
  public chartSeriesData:Array<any> = [];
  chartOptions:Object;

  constructor() { 
    this.chartInstance = new EventEmitter<any>();
  }

  ngOnInit() {
    console.log("xaxis data is ",this.lineChartAxisData);
    
    // this.chartName = this.lineChartInfo.chartName;
    this.chartSeriesData = this.lineChartInfo;
    // this.chartSeriesData = [{
    //   data: [-10, -5, 0, 5, 10, 15, 10, 10, 5, 0, -5],
    //   zones: [{
    //       value: 0,
    //       color: '#f7a35c'
    //   }, {
    //       value: 10,
    //       color: '#7cb5ec'
    //   }, {
    //       color: '#90ed7d'
    //   }]
    // }];
    //console.log('Afterchange data : ', this.chartSeriesData);
    this.chartOptions = {
      chart: {
          type: 'line',
          backgroundColor:"#2d3944",
         // width:this.lineChartInfo.width,
          height:300
      },
      xAxis: {
         categories: this.lineChartAxisData['xCategory']
      },
      yAxis: {
        
      },
      title : { 
        text :"Load cell Live mapping ",
        style:{
          color:"#d3d6d8"
        }
      },
      threshold: 15,
      credits: {
          enabled: false
      },
      exporting: { enabled: false },
      series: [{
        lineWidth: 3,
        marker: {
            enabled: false
          },
          type: 'coloredline',
          data:this.chartSeriesData 
      }]
    };
  }
  getChartInstance(corrInstance){
    this.chartInstance.emit(corrInstance)
}

}