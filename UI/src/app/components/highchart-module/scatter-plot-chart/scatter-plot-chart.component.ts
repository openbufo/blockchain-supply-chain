import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-scatter-plot-chart',
  templateUrl: './scatter-plot-chart.component.html',
  styleUrls: ['./scatter-plot-chart.component.css']
})
export class ScatterPlotChartComponent implements OnInit {
  chartOptions:Object;
  @Input() chartData;
  constructor() { }

  ngOnInit() {
    this.chartOptions = {
      chart: {
        defaultSeriesType: 'scatter'
      },
      title: {
        text: ' '
      },
      xAxis: {
          //categories: [2,3,4,6,7,9,10,12],
          showEmpty: false,
          title:{
            text: 'RUN_TIME'
          }
      },
      yAxis: {
          showEmpty: false,
          title:{
            text: 'MEMORY_FOOTPRINT_GB'
          }
      },
      series: [{
          allowPointSelect: true,
          data: this.chartData.dataScatter[0],
          showInLegend: false
      }],
      credits:{
        enabled:false
      },
      exporting: {
        enabled: false
      },
      tooltip: {
        formatter: function(){

          return '<b>RUN_TIME:</b>'+this.x+'<br><b>MEMORY_FOOTPRINT_GB</b>'+this.y;
        }
      }
    }
  }
}
