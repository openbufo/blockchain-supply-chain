import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Logger }  from 'angular2-logger/core';

import {
  loginModel,
  LoginService,
  TOKEN
} from '../../services/login.service';
import { StorageService } from '../../services/storage.service';
import { AppSettings } from '../../configuration/AppSettings';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginService, StorageService, Logger]
})
export class LoginComponent implements OnInit {
  public loggedIn:TOKEN;
  public errorMsg:string =null;
  public model = new loginModel('','');
  constructor(
    private _router: Router,
    private _service: LoginService,
    private _storage: StorageService,
    private _logger: Logger
  ){

  }
  ngOnInit() {
    this.model.principal_user = '';
    this.model.principal_user = '';
    console.log(this.loggedIn);
  }
  onLoggedin() {
    //localStorage.setItem('isLoggedin', 'true');
    this._logger.info("starting logging in");
    // if(this.model.userName==='Admin'){
    //   if(this.model.password==='admin'){
        this._service.login(this.model)
          .subscribe(
              data => {
                  // console.log(data);
                  // this.loggedIn = data.token;
                  this._storage.api.local.save('currentUser', data);    
                  this._storage.api.local.save('userType', this.model.principal_user);     
                  this._router.navigate(['/landing']);
              },
              message => {
                this._logger.error(message);
                this.errorMsg = message;
              }
          );
    //   }else{
    //     this.errorMsg = "User and Password does not match";
    //   }
    // }else{
    //   this.errorMsg = "User does not exist";
    //   this._logger.error("invalid credentials");
    // }
  }
}
