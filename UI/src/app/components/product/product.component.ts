import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService} from '../../services/dashboard.service';
import { StorageService} from '../.../../../services/storage.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor( private _router:Router,private _dashboardService:DashboardService,
  private _storage:StorageService) { }

  tableData;

  ngOnInit() {
    try {
      let userDetails = this._storage.api.local.get("currentUser");
      this._dashboardService.getProductSearchData(userDetails).subscribe(result => {
        if(result){
           this.tableData = result.content;
         }
       });
    } catch (error) {
      console.log(error);
    }
  }

  navigateToDashboard(){
    this._router.navigate(['/landing/dashboard']);
  }
}
