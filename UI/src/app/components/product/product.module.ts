import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductComponent } from './product.component';
import { KpmgGridModule} from '../shared/kpmg-grid/kpmg-grid.module'
import { ProductPageRoutingModule} from './product-routing/product.route'
import { DashboardService} from '../../services/dashboard.service';

@NgModule({
  imports: [
    CommonModule,
    ProductPageRoutingModule,
    KpmgGridModule
  ],
  declarations: [ProductComponent],
  providers:[DashboardService]
})
export class ProductModule { }
