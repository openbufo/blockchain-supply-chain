import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers } from "@angular/http";
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { StorageService } from './storage.service';
import { AppSettings } from '../configuration/AppSettings';
import { AuthHttpService } from './auth-http.service';


@Injectable()
export class GeoDirectionService {
    constructor(
        public _http: AuthHttpService,
        public http: Http,
        private _storage: StorageService) {

    }
    
    private subject = new Subject<any>();

    public getGeoDirection() {

        return this.http.get('assets/jsons/' + 'direction.json')
            .map((response) => response.json())
            .catch(this._http.handleError);
    }

    setMapData(value){
        console.log("called ",value);
        this.subject.next({mapData: value}); 
      }
    
      getMapData():Observable<any> {
          console.log("get it back");
          return this.subject.asObservable();
      }
}
