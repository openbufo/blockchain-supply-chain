import { TestBed, inject } from '@angular/core/testing';

import { AllsetupService } from './allsetup.service';

describe('AllsetupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AllsetupService]
    });
  });

  it('should be created', inject([AllsetupService], (service: AllsetupService) => {
    expect(service).toBeTruthy();
  }));
});
