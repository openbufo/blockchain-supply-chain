import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import { Router } from '@angular/router';
import {AuthHttpService} from './auth-http.service';
import {AppSettings} from '../configuration/AppSettings';
import { StorageService } from './storage.service';

@Injectable()
export class CommonHttpService
{
   constructor(public _http: AuthHttpService,
                public _router:Router,
                public _storage: StorageService){

                    /**
                     * this.expiresIn= JSON.parse(localStorage.getItem("token")).expires_in;
                     */
                }
    commonHttpGetRequest(URL,headers) {
        if(headers){
            return this._http.get(URL,headers)
            .map((response=> response.json()))
            .catch((err:any) => {
                console.log(err);
                console.log(err.status);
                //console.log(err.statusText);
                if (err.status == 401 || err.status == 403 ||err.status == 302|| err.status==404 || err.status==500 || err.status==503) { 
                    // this._storage.api.local.clear();
                    // this._storage.api.session.clear();
                    // window.location.reload();
                  return Observable.empty();
                }else {
                  return Observable.throw(err.json().error || 'Server error');
                }
            });
        }else{
            return this._http.get(URL)
            .map((response=> response.json()))
            .catch((err:any) => {
                console.log(err);
                console.log(err.status);
                if (err.status == 401 || err.status == 403 ||err.status == 302 || err.status==404 || err.status==500 || err.status==503) { 
                    // this._storage.api.local.clear();
                    // this._storage.api.session.clear();
                    // window.location.reload();
                  return Observable.empty();
                }else {
                  return Observable.throw(err.json().error || 'Server error');
                }
            });
        }
        
    }
    /**expiresIn:number */
    commonHttpPostRequest(URL,data,headers) {
        /**
         *   this.expiresIn = JSON.parse(localStorage.getItem("token")).expires_in;
         *   setTimeOut(function(){ 
         *   this._storage.api.local.clear();
             this._storage.api.session.clear();
             window.location.reload();
         *   },this.expiresIn);
         */
        if(headers){
            return this._http.post(URL,data,headers)
            .map((response=> response.json()))
            .catch((err:any) => {
                console.log(err);
                console.log(err.status);
               // console.log(err.statusText);
                if (err.status == 401 || err.status == 403 ||err.status == 302 || err.status==404 || err.status==500 || err.status==503) { 
                    // this._storage.api.local.clear();
                    // this._storage.api.session.clear();
                    // window.location.reload();
                  return Observable.empty();
                }else {
                  return Observable.throw(err.json().error || 'Server error');
                }
            });
        }else{
            return this._http.post(URL,data)
            .map((response=> response.json()))
            .catch((err:any) => {
                console.log(err);
                console.log(err.status);
                if (err.status == 401 || err.status == 403 ||err.status == 302 || err.status==404 || err.status==500 || err.status==503) { 
                    // this._storage.api.local.clear();
                    // this._storage.api.session.clear();
                    // window.location.reload();
                  return Observable.empty();
                }else {
                  return Observable.throw(err.json().error || 'Server error');
                }
            });
        }
    }
}

