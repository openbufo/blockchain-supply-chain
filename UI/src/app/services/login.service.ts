import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers } from "@angular/http";
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

import { StorageService } from './storage.service';
import { AppSettings } from '../configuration/AppSettings';
import { AuthHttpService } from './auth-http.service';

export class loginModel{
  public principal_user :string;
  public principal_secret:string;
  constructor( userName :string, password:string){
    this.principal_user = userName;
    this.principal_secret = password; 
  }   
}
export interface TOKEN {
  access_token:string;
  token_type:string;
  expires_in:string;
  userName:string;
  roles:string;
  '.issued':string;
  '.expires':string;
}
@Injectable()
export class LoginService {
  constructor(
    public _http: AuthHttpService,
    public http:Http,
    private _storage:StorageService) { 

  }
  public login(model:loginModel){
    // return this._http.get(AppSettings.URL.LOGIN_SERVICE)
    // .map((response=> response.json()))
    // .catch(this.handleError);
    let headers = new Headers();
    headers.append('content-type', 'application/x-www-form-urlencoded');

    /*return this._http.post("/kpmg/dfbc/login/", {"principal_user":model.principal_user, "principal_secret": window.btoa(model.principal_secret)})
    .map((response)=>response.json())
    .catch(this._http.handleError);*/
    let file_name = "miner_user";
    if(model.principal_user === "transporter"){
      file_name = "transporter_user"
    }
    if(model.principal_user === "customer"){
      file_name = "customer_user"
    }

    if(model.principal_user === "auditor"){
      file_name = "auditor_user"
    }

    return this.http.get('assets/jsons/'+file_name+'.json')
    .map((response)=>response.json())
    .catch(this._http.handleError);

  }
  public isAuthenticated():boolean {
    let token = this._storage.api.local.get('currentUser');
    return token != null && !!token.principal_user; //TODO check empty token
  }
  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }      
}
