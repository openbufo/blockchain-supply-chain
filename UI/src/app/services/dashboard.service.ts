import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers } from "@angular/http";
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { StorageService } from './storage.service';
import { AppSettings } from '../configuration/AppSettings';
import { AuthHttpService } from './auth-http.service';     


@Injectable()
export class DashboardService {
  private subject = new Subject<any>();
  constructor(
    public _http: AuthHttpService,
    public http:Http,
    private _storage:StorageService) { 

  }



  public getDashboardData(URL,userDetails){
    let headers = new Headers();
    headers.append('content-type', 'application/x-www-form-urlencoded');
    let file_name = "dashboard_mfgr";
    if(userDetails.principal_user === "dsp_user"){
      file_name = "dashboard_dsp"
    }
    if(userDetails.principal_user === "dst_user"){
      file_name = "dashboard_dst"
    }
    return this.http.get('assets/jsons/'+file_name+'.json')
    .map((response)=>response.json())
    .catch(this._http.handleError);

    // return this.http.post(URL,userDetails)
    // .map((response)=>response.json())
    // .catch(this._http.handleError);

  }

  public getProductSearchData(userDetails){
    return this.http.get('assets/jsons/'+'product_search.json')
    .map((response)=>response.json())
    .catch(this._http.handleError);
    // return this.http.post(AppSettings.URL.PRODUCTS,userDetails)
    // .map((response)=>response.json())
    // .catch(this._http.handleError);
  }

  public getAuditData(userDetails){
    return this.http.get('assets/jsons/'+'auditData.json')
    .map((response)=>response.json())
    .catch(this._http.handleError);
    // return this.http.post(AppSettings.URL.PRODUCTS,userDetails)
    // .map((response)=>response.json())
    // .catch(this._http.handleError);
  }

  public getViewVerificationsData(userDetails){
    // return this.http.post(AppSettings.URL.VIEW_VERIFICATIONS,userDetails)
    // .map((response)=>response.json())
    // .catch(this._http.handleError);

    return this.http.get('assets/jsons/'+'view_verifications.json')
    .map((response)=>response.json())
    .catch(this._http.handleError);
  }

  public getNotificationsList(userDetails){
    // return this.http.post(AppSettings.URL.NOTIFICATIONS_LIST,userDetails)
    // .map((response)=>response.json())
    // .catch(this._http.handleError);

    return this.http.get('assets/jsons/'+'notifications_list.json')
    .map((response)=>response.json())
    .catch(this._http.handleError);
  }

  public getNotificationsDetails(inputJson){
    // return this.http.post(AppSettings.URL.NOTIFICATION_DETAIL,inputJson)
    // .map((response)=>response.json())
    // .catch(this._http.handleError);

    return this.http.get('assets/jsons/'+'notifications_details.json')
    .map((response)=>response.json())
    .catch(this._http.handleError);
  }

  public isAuthenticated():boolean {
    let token = this._storage.api.local.get('currentUser');
    return token != null && !!token.principal_user; //TODO check empty token
  }
  private handleError(error: Response) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
  }      
}
