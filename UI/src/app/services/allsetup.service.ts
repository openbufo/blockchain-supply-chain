import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import { Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {HttpClient} from '@angular/common/http';
import { StorageService } from './storage.service';
import { Router } from '@angular/router';

import { AppSettings } from '../configuration/AppSettings';

@Injectable()
export class AllsetupService {
  headers: Headers;
  options: RequestOptions;

  constructor(public _http: Http, private _storage: StorageService, private _router: Router) {
    this.headers = new Headers({'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8', 'Accept': 'q=0.8;application/json;q=0.9' });
    this.options = new RequestOptions({ headers: this.headers });
  }

  comonnonGetService(url){
    return this._http.get(url)
    .map((res:Response) => res.json())
    // .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    .catch((err:any) => {
      if (err.status == 401 || err.status == 403) { 
        this._storage.api.session.remove(AppSettings.SESSION_KEY.TOKEN);
        this._router.navigate(['/login']);
        return Observable.empty();
      }else {
        return Observable.throw(err.json().error || 'Server error');
      }
    });
  }
  comonnonPostService(url, input){
    input = window.btoa(JSON.stringify(input));
    // input = window.btoa(decodeURI(encodeURIComponent(JSON.stringify(input))));
    return this._http.post(url, input, this.options)
    .map((res:Response) => res.json())
    // .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    .catch((err:any) => {
      console.log(err);
      if (err.status == 401 || err.status == 403) { 
        this._storage.api.session.remove(AppSettings.SESSION_KEY.TOKEN);
        this._router.navigate(['/login']);
        return Observable.empty();
      }else {
        return Observable.throw(err.json().error || 'Server error');
      }
});
}
  getSearchFilterService(){
    return this._http.get('assets/jsons/selectData2.json')
    // return this._http.get('http://172.30.0.102:13131/selectdata')
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }
  getTableDataService(){
    return this._http.get('assets/jsons/tableData2.json')
    .map((res:Response) => res.json())
    .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  // getTableDataService2(body){
  //   let bodyString = JSON.stringify(body);
  //   return this._http.post('http://172.30.0.102:13131/tableData', bodyString, this.options)
  //   .map((res:Response) => res.json())
  //   .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  // }



 
}
